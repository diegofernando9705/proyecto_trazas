<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablaClientesRemisionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabla_clientes_remisiones', function (Blueprint $table) {
            $table->id();
            $table->integer('tipo_id_cliente');
            $table->string('identificacion', 250);
            $table->longText('nombres_cliente');
            $table->longText('apellidos_cliente');            
            $table->longText('direccion_cliente');
            $table->longText('ciudad_cliente');
            $table->string('telefono_fijo_cliente', 11)->nullable();
            $table->string('celular_1_cliente', 12);
            $table->string('celular_2_cliente', 12)->nullable();
            $table->string('email_cliente', 100)->unique();
            $table->integer('estado_cliente');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabla_clientes_remisiones');
    }
}
