<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablaConductorRemisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabla_conductor_remision', function (Blueprint $table) {
            $table->id();
            $table->integer('id_tipo_conductor');
            $table->string('identificacion_conductor', 250);
            $table->longText('nombres_conductor');
            $table->longText('apellidos_conductor');
            $table->string('celular_1_conductor', 12);
            $table->string('celular_2_conductor', 12)->nullable();
            $table->string('placa_conductor', 12);
            $table->integer('estado_conductor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabla_conductor_remision');
    }
}
