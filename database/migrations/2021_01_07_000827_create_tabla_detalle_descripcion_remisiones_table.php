<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablaDetalleDescripcionRemisionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabla_detalle_descripcion_remisiones', function (Blueprint $table) {
            $table->id();
            $table->integer('id_remision_detalle');
            $table->integer('id_descripcion_remisiones');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabla_detalle_descripcion_remisiones');
    }
}
