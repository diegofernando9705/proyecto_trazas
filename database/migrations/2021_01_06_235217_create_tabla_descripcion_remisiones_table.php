<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablaDescripcionRemisionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('tabla_descripcion_remisiones', function (Blueprint $table) {
            $table->increments('id_descripcion');
            $table->integer('id_producto');
            $table->longText('certificado_de_origen');
            $table->integer('cantidad')->nullable();
            $table->string('total', 250)->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabla_descripcion_remisiones');
    }
}
