<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablaParametrosRemisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabla_parametros_remision', function (Blueprint $table) {
            $table->id();
            $table->integer('id_tipo_parametro');
            $table->string('identificacion_parametros', 250);
            $table->longText('logo_parametros');
            $table->longText('nombre_parametros');
            $table->longText('direccion_parametros');
            $table->longText('id_ciudad_parametros');
            $table->string('telefono_fijo_parametros', 12)->nullable();
            $table->string('celular_1_parametros', 12);
            $table->string('celular_2_parametros', 12)->nullable();
            $table->string('celular_3_parametros', 12)->nullable();
            $table->string('email_cliente', 100)->unique();
            $table->string('estado_parametro', 12);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabla_parametros_remision');
    }
}
