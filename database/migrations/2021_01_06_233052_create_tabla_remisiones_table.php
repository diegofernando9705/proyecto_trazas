<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablaRemisionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabla_remisiones', function (Blueprint $table) {
            $table->increments('id_consecutivo');
            $table->integer('id_cliente');
            $table->string('fecha', 250)->nullable();
            $table->string('vence', 250)->nullable();
            $table->longText('orden');
            $table->integer('id_conductor_remision');
            $table->string('subtotal', 250)->nullable();
            $table->string('total', 250)->nullable();
            $table->integer('estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabla_remisiones');
    }
}
