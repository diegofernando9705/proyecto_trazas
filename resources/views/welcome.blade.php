<!-- charts -->
                    <div class="chart">
                        <div class="row">
                            <div class="col-lg-6 pr-lg-2 chart-grid">
                                <div class="card text-center card_border">
                                    <div class="card-header chart-grid__header">
                                        Bar Chart
                                    </div>
                                    <div class="card-body">
                                        <!-- bar chart -->
                                        <div id="container">
                                            <canvas id="barchart">
                                            </canvas>
                                        </div>
                                        <!-- //bar chart -->
                                    </div>
                                    <div class="card-footer text-muted chart-grid__footer">
                                        Updated 2 hours ago
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 pl-lg-2 chart-grid">
                                <div class="card text-center card_border">
                                    <div class="card-header chart-grid__header">
                                        Line Chart
                                    </div>
                                    <div class="card-body">
                                        <!-- line chart -->
                                        <div id="container">
                                            <canvas id="linechart">
                                            </canvas>
                                        </div>
                                        <!-- //line chart -->
                                    </div>
                                    <div class="card-footer text-muted chart-grid__footer">
                                        Updated just now
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- chatting -->
                    <div class="data-tables">
                        <div class="row">
                            <div class="col-lg-12 chart-grid mb-4">
                                <div class="card card_border p-4">
                                    <div class="card-header chart-grid__header pl-0 pt-0">
                                        Chatting
                                    </div>
                                    <div class="messaging">
                                        <div class="inbox_msg">
                                            <div class="inbox_people">
                                                <div class="headind_srch">
                                                    <div class="srch_bar">
                                                        <div class="stylish-input-group">
                                                            <input class="search-bar" placeholder="Search Chat" type="text">
                                                                <span class="input-group-addon">
                                                                    <button type="button">
                                                                        <i aria-hidden="true" class="fa fa-search">
                                                                        </i>
                                                                    </button>
                                                                </span>
                                                            </input>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="inbox_chat">
                                                    <div class="chat_list active_chat">
                                                        <div class="chat_people">
                                                            <div class="chat_img">
                                                                <img alt="Alexander" class="img-fluid" src="assets/images/avatar5.jpg">
                                                                </img>
                                                            </div>
                                                            <div class="chat_ib">
                                                                <h5>
                                                                    Alexander
                                                                    <span class="chat_date">
                                                                        1 hour ago
                                                                    </span>
                                                                </h5>
                                                                <p>
                                                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="chat_list">
                                                        <div class="chat_people">
                                                            <div class="chat_img">
                                                                <img alt="Anderson" class="img-fluid" src="assets/images/avatar3.jpg">
                                                                </img>
                                                            </div>
                                                            <div class="chat_ib">
                                                                <h5>
                                                                    Anderson
                                                                    <span class="chat_date">
                                                                        5 hours ago
                                                                    </span>
                                                                </h5>
                                                                <p>
                                                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="chat_list">
                                                        <div class="chat_people">
                                                            <div class="chat_img">
                                                                <img alt="Isabella" class="img-fluid" src="assets/images/avatar5.jpg">
                                                                </img>
                                                            </div>
                                                            <div class="chat_ib">
                                                                <h5>
                                                                    Isabella
                                                                    <span class="chat_date">
                                                                        Yesterday
                                                                    </span>
                                                                </h5>
                                                                <p>
                                                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="chat_list">
                                                        <div class="chat_people">
                                                            <div class="chat_img">
                                                                <img alt="Charlotte" class="img-fluid" src="assets/images/avatar4.jpg">
                                                                </img>
                                                            </div>
                                                            <div class="chat_ib">
                                                                <h5>
                                                                    Charlotte
                                                                    <span class="chat_date">
                                                                        Mar 04
                                                                    </span>
                                                                </h5>
                                                                <p>
                                                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="chat_list">
                                                        <div class="chat_people">
                                                            <div class="chat_img">
                                                                <img alt="Davidson" class="img-fluid" src="assets/images/avatar2.jpg">
                                                                </img>
                                                            </div>
                                                            <div class="chat_ib">
                                                                <h5>
                                                                    Davidson
                                                                    <span class="chat_date">
                                                                        Feb 18
                                                                    </span>
                                                                </h5>
                                                                <p>
                                                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="chat_list">
                                                        <div class="chat_people">
                                                            <div class="chat_img">
                                                                <img alt="Elexa ker" class="img-fluid" src="assets/images/avatar1.jpg">
                                                                </img>
                                                            </div>
                                                            <div class="chat_ib">
                                                                <h5>
                                                                    Elexa ker
                                                                    <span class="chat_date">
                                                                        Feb 04
                                                                    </span>
                                                                </h5>
                                                                <p>
                                                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="chat_list">
                                                        <div class="chat_people">
                                                            <div class="chat_img">
                                                                <img alt="Charlotte" class="img-fluid" src="assets/images/avatar4.jpg">
                                                                </img>
                                                            </div>
                                                            <div class="chat_ib">
                                                                <h5>
                                                                    Charlotte
                                                                    <span class="chat_date">
                                                                        Jan 28
                                                                    </span>
                                                                </h5>
                                                                <p>
                                                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mesgs">
                                                <div class="msg_history">
                                                    <div class="incoming_msg">
                                                        <div class="incoming_msg_img">
                                                            <img alt="Alexander" class="img-fluid" src="assets/images/avatar5.jpg">
                                                            </img>
                                                        </div>
                                                        <div class="received_msg">
                                                            <div class="received_withd_msg">
                                                                <p>
                                                                    Coming along nicely, we've got a Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                                </p>
                                                                <span class="time_date">
                                                                    10:05 AM | Mar 9
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="outgoing_msg">
                                                        <div class="sent_msg">
                                                            <p>
                                                                Great start, I've added some Lorem ipsum dolor sit amet.
                                                            </p>
                                                            <span class="time_date">
                                                                12:15 PM | Mar 9
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="incoming_msg">
                                                        <div class="incoming_msg_img">
                                                            <img alt="Alexander" class="img-fluid" src="assets/images/avatar5.jpg">
                                                            </img>
                                                        </div>
                                                        <div class="received_msg">
                                                            <div class="received_withd_msg">
                                                                <p>
                                                                    Sed ut perspiciatis unde omnis iste natus error sit
                                                                </p>
                                                                <span class="time_date">
                                                                    09:16 AM | Yesterday
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="outgoing_msg">
                                                        <div class="sent_msg">
                                                            <p>
                                                                But I must explain to you.
                                                            </p>
                                                            <span class="time_date">
                                                                03:15 PM | Today
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="incoming_msg">
                                                        <div class="incoming_msg_img">
                                                            <img alt="Alexander" class="img-fluid" src="assets/images/avatar5.jpg">
                                                            </img>
                                                        </div>
                                                        <div class="received_msg">
                                                            <div class="received_withd_msg">
                                                                <p>
                                                                    At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium
                            voluptatum deleniti atque corrupti quos dolores.
                                                                </p>
                                                                <span class="time_date">
                                                                    03:16 PM | Today
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="type_msg">
                                                    <div class="input_msg_write">
                                                        <input class="write_msg" placeholder="Type a message" type="text"/>
                                                        <button class="msg_send_btn" type="button">
                                                            <i aria-hidden="true" class="fa fa-paper-plane-o">
                                                            </i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- //chatting -->

                        <!-- accordions -->
                    <div class="accordions">
                        <div class="row">
                            <!-- accordion style 1 -->
                            <div class="col-lg-12 mb-4">
                                <div class="card card_border">
                                    <div class="card-header chart-grid__header">
                                        Bootstrap Accordions
                                    </div>
                                    <div class="card-body">
                                        <div class="accordion" id="accordionExample">
                                            <div class="card">
                                                <div class="card-header bg-white p-0" id="headingOne">
                                                    <a aria-controls="collapseOne" aria-expanded="true" class="card__title p-3" data-target="#collapseOne" data-toggle="collapse" href="#">
                                                        Collapsed accordion heading
                                                    </a>
                                                </div>
                                                <div aria-labelledby="headingOne" class="collapse show" data-parent="#accordionExample" id="collapseOne">
                                                    <div class="card-body para__style">
                                                        Nulla tincidunt quam justo, in tincidunt tortor sollicitudin a. Donec porta posuere
                      libero sed varius. Phasellus hendrerit commodo sem, at sagittis sapien semper quis.
                      Etiam vitae facilisis nibh. Maecenas erat nisl, blandit at nunc a, lobortis sagittis
                      ex. Maecenas pharetra pulvinar tincidunt.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header bg-white p-0" id="headingTwo">
                                                    <a aria-controls="collapseTwo" aria-expanded="false" class="card__title p-3" data-target="#collapseTwo" data-toggle="collapse" href="#">
                                                        Click here to collapse accordion
                                                    </a>
                                                </div>
                                                <div aria-labelledby="headingTwo" class="collapse" data-parent="#accordionExample" id="collapseTwo">
                                                    <div class="card-body para__style">
                                                        Nulla tincidunt quam justo, in tincidunt tortor sollicitudin a. Donec porta posuere
                      libero sed varius. Phasellus hendrerit commodo sem, at sagittis sapien semper quis.
                      Etiam vitae facilisis nibh. Maecenas erat nisl, blandit at nunc a, lobortis sagittis
                      ex. Maecenas pharetra pulvinar tincidunt.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header bg-white p-0" id="headingThree">
                                                    <a aria-controls="collapseThree" aria-expanded="false" class="card__title p-3" data-target="#collapseThree" data-toggle="collapse" href="#">
                                                        Click here to
                      collapse accordion
                                                    </a>
                                                </div>
                                                <div aria-labelledby="headingThree" class="collapse" data-parent="#accordionExample" id="collapseThree">
                                                    <div class="card-body para__style">
                                                        Nulla tincidunt quam justo, in tincidunt tortor sollicitudin a. Donec porta posuere
                      libero sed varius. Phasellus hendrerit commodo sem, at sagittis sapien semper quis.
                      Etiam vitae facilisis nibh. Maecenas erat nisl, blandit at nunc a, lobortis sagittis
                      ex. Maecenas pharetra pulvinar tincidunt.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- //accordion style 1 -->
                        </div>
                    </div>
                    <!-- //accordions -->

                    
                    <!-- modals -->
                    <section class="template-cards">
                        <div class="card card_border">
                            <div class="cards__heading">
                                <h3>
                                    Modals -
                                    <span>
                                        2 different types of bootstrap modals
                                    </span>
                                </h3>
                            </div>
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-lg-6 pr-lg-2 chart-grid">
                                        <div class="card text-center card_border">
                                            <div class="card-header chart-grid__header">
                                                Demo modal
                                            </div>
                                            <div class="card-body">
                                                <!-- Button trigger modal -->
                                                <button class="btn btn-primary btn-style" data-target="#exampleModal" data-toggle="modal" type="button">
                                                    Launch demo
                                                </button>
                                                <!-- Modal -->
                                                <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="exampleModal" role="dialog" tabindex="-1">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">
                                                                    Modal title
                                                                </h5>
                                                                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                                                                    <span aria-hidden="true">
                                                                        ×
                                                                    </span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                ...
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn btn-danger" data-dismiss="modal" type="button">
                                                                    Close
                                                                </button>
                                                                <button class="btn btn-success" type="button">
                                                                    Save changes
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 chart-grid">
                                        <div class="card text-center card_border">
                                            <div class="card-header chart-grid__header">
                                                Vertical centered
                                            </div>
                                            <div class="card-body">
                                                <!-- Button trigger modal -->
                                                <button class="btn btn-primary btn-style" data-target="#exampleModalCenter" data-toggle="modal" type="button">
                                                    Launch demo
                                                </button>
                                                <!-- Modal -->
                                                <div aria-hidden="true" aria-labelledby="exampleModalCenterTitle" class="modal fade" id="exampleModalCenter" role="dialog" tabindex="-1">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLongTitle">
                                                                    Modal title
                                                                </h5>
                                                                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                                                                    <span aria-hidden="true">
                                                                        ×
                                                                    </span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                ...
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn btn-danger" data-dismiss="modal" type="button">
                                                                    Close
                                                                </button>
                                                                <button class="btn btn-success" type="button">
                                                                    Save changes
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- //modals -->