<div class="row">

    <div class="fechas col-12 col-md-4 col-lg-4 col-sm-4 col-xl-4">
        <div class="row">
            <b class="col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">B&uacute;squeda por Nombre:</b>
            <hr />
            <div class="form-group col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">
                <input type="text" class="form-control" name="nombre" id="nombre">
            </div>
        </div>
    </div>

    <div class="fechas col-12 col-md-4 col-lg-4 col-sm-4 col-xl-4">
        <div class="row">
            <b class="col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">B&uacute;squeda por Apellido:</b>
            <hr />
            <div class="form-group col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">
                <input type="text" class="form-control" name="apellido" id="apellido">
            </div>
        </div>
    </div>

    <div class="fechas col-12 col-md-4 col-lg-4 col-sm-4 col-xl-4">
        <div class="row">
            <b class="col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">B&uacute;squeda por correo:</b>
            <hr />
            <div class="form-group col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">
                <input type="text" class="form-control" name="correo" id="correo">
            </div>
        </div>
    </div>

    <div class="fechas col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">

        <div class="row">
            <div class="form-group col-12 col-md-6 col-lg-6 col-sm-6 col-xl-6">
                <b class="col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">Regionales:</b>
                <select class="selectpicker col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12 " data-actions-box="true"  data-live-search="true" name="regional" id="regional" placeholder="Seleccione una opcion">
                    @foreach($regionales as $regional)
                    <option value="{{ $regional->id }}">{{ $regional->nombre }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group col-12 col-md-6 col-lg-6 col-sm-6 col-xl-6">
                <b class="col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">Asociados:</b>
                <select class="selectpicker col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12 " multiple data-actions-box="true"  data-live-search="true" id="asociado" name="asociado[]">
                    <option value="1">Asociado</option>
                    <option value="2">No asociado</option>
                </select>
            </div>
        </div>

    </div>
    <hr>
    <div class="tabla  col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">
        <table class="table table-bordered table-hover">
            <thead>
            <th>
                Identificación
            </th>
            <th>
                Nombre
            </th>
            <th>
                Apellido
            </th>
            <th>
                Correo
            </th>
            <th>
                Regional
            </th>
            <th>
                Asociado
            </th>
            </thead>
            <tbody id="resultadoConsulta">

            </tbody>
        </table>

    </div>
</div>

<center>
    <div class="row">
        <form action="{{ url('/exportar/csv/accesos')}}" method="POST" class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">

            @csrf
            @method('POST')


            <input type="hidden" name="nombreCSV" id="nombreFormCSV" value="">

            <input type="hidden" name="apellidoCSV" id="apellidoFormCSV" value="">
            <input type="hidden" name="correoCSV" id="correoFormCSV" value="">
            <input type="hidden" name="regionalCSV" id="regionalFormCSV" value="1">
            <input type="hidden" name="asociadoCSV" id="asociadoFormCSV" value="0">


            <button type="submit" class="btn btn-info">Exportar CSV</button>

        </form>
        <button action="{{ url('/exportar/generate/accesos') }}" type="button" class="btn btn-success btn-buscar-accesos float-right col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">Buscar</button>
        <form action="{{ url('/exportar/pdf/accesos')}}" method="POST"  class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">

            @csrf
            @method('POST')


            <input type="hidden" name="nombrePDF" id="nombreFormPDF" value="">

            <input type="hidden" name="apellidoPDF" id="apellidoFormPDF" value="">
            <input type="hidden" name="correoPDF" id="correoFormPDF" value="">
            <input type="hidden" name="regionalPDF" id="regionalFormPDF" value="1">
            <input type="hidden" name="asociadoPDF" id="asociadoFormPDF" value="0">


            <button type="submit" class="btn btn-danger">Exportar PDF</button>

        </form>
    </div>
</center>

<script type="text/javascript">

    $(function () {
        $('.selectpicker').selectpicker();
    });

</script>