<div class="row">
    <div class="fechas col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">
        <div class="row">
            <b class="col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">B&uacute;squeda por fechas:</b>
            <hr />
            <div class="form-group col-6 col-md-6 col-lg-6 col-sm-6 col-xl-6">
                <input type="date" class="form-control" name="fechaInicial" id="fechaInicial">
                <em>Fecha inicial</em>
            </div>
            <div class="form-group col-6 col-md-6 col-lg-6 col-sm-6 col-xl-6">
                <input type="date" class="form-control" name="fechaFinal" id="fechaFinal">
                <em>Fecha Final</em>
            </div>
        </div>
    </div>
    <hr>
    <div class="tabla  col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">
        <table class="table table-bordered table-hover">
            <thead>
            <th>
                Identificación
            </th>
            <th>
                Nombre
            </th>
            <th>
                Descripcion
            </th>
            <th>
                Conferencia
            </th>

            </thead>
            <tbody id="resultadoConsulta">

            </tbody>
        </table>

    </div>
</div>

<center>
    <div class="row">
        
        <form action="{{ route('reportes.export.mensajes')}}" method="POST" class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">

            @csrf
            @method('POST')

            <input type="hidden" name="fechaInicial" id="fechaInicialForm" value="">
            <input type="hidden" name="fechaFinal" id="fechaFinalForm" value="">


            <button type="submit" class="btn btn-info exportCSVMensajes">Exportar CSV</button>

        </form>
        <button type="button" class="btn btn-success btn-buscar-mensajes float-right col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">Buscar</button>
        <form action="{{ route('reportes.export.mensajespdf')}}" method="POST" class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">

            @csrf
            @method('POST')

            <input type="hidden" name="fechaInicial" id="fechaInicialPDF" value="">
            <input type="hidden" name="fechaFinal" id="fechaFinalPDF" value="">


            <button type="submit" class="btn btn-danger exportPDFMensajes">Exportar PDF</button>

        </form>
    </div>
</center>

<script type="text/javascript">

    $(function () {
        $('.selectpicker').selectpicker();
    });

</script>
