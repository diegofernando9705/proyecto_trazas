<?php $i = 0; ?>
@foreach($resultado as $value)
    @foreach($value as $total)
        <tr>
            <td>
                {{ $total->identificacion }}
            </td>
            <td>
                {{ $total->nombre }}
            </td>
            <td>
                {{ $total->apellido }}
            </td>
            
            <td>
                {{ $total->email }}
            </td>
            
            <td>
                {{ $total->nombreRegional }}
            </td>
            
            <td>
                @if($total->asociado == 1)
                Asociado
                @else
                No Asociado
                @endif
            </td>
    @endforeach

@endforeach
<tr>
    <td colspan="2"><b>Total registros: </b>{{ $i++ }} registros</td>
</tr>
