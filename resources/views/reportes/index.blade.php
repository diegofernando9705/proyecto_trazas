@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h2>Mentoría</h2></div>
                <div class="card-body">
                    @include('custom.message')

                    <form id="formTodo" data-action="{{ route('reportes.generate')}}" method="POST">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        @csrf
                        @method('post')
                        <div class="form-group">
                            <label>
                                <b>
                                    Selecciona una opci&oacute;n para generar un reporte
                                </b>
                            </label>
                            <select class="form-control tablaSeleccionada"  name="tabla" id="tabla">
                                <option value="0">Seleccionar una Tabla</option>
                                <option value="todo">Todo</option>
                                <option value="accesos">Tabla accesos</option>
                                <!--
                                <option value="accesos_historia">Tabla Historial de Accesos</option>-->
                                <option value="mensajes">Tabla mensajes</option>
                            </select>
                        </div>
                        
                    </form>
                    <div class="camposReportes col-12 col-sm-12 col-md-12 col-lg-12  col-xl-12" id="camposReportes">
                            <div class="alert alert-warning">Debe seleccionar una opci&oacute;n para generar el reporte</div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
