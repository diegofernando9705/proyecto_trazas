<div class="row" style="border: 1px solid #E7EDF6; padding-top: 20px; padding-bottom: 20px; margin-bottom: 20px;">
	<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
		<img src="{{ asset('img/icon-trazas.png') }}" width="100%" />
	</div>
	<div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8" style="border-left: 1px solid #E7EDF6; border-right: 1px solid #E7EDF6;">
		<p align="justify" style="text-align: center; font-weight: bold; line-height:18px;">
			TRAZAS Y MINERALES DE COLOMBIA S.A.S <br>
			Nit: 901.163.520-6 Régimen Común <br>
			PLANTA PRINCIPAL PASO DE LA BOLSA, VEREDA SANCHEZ / CALLEJON LA LUISA Cel: 315 315 7478 - 316 282 0912 <br>
			dianagomez@trazascol.com - info@trazascol.com <br>
			JAMUNDI - VALLE
		</p>
	</div>
	<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
		<p align="justify" style="text-align: center; font-weight: bold; line-height:18px;">
			<?php echo date('d-m-Y'); ?>
		</p>
	</div>
</div>
