<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Sección principal | Trazas y Minerales de Colombia</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

  <link href="{{ asset('bootstrap4/css/style-starter.css') }}" rel="stylesheet" />
  <link href="{{ asset('bootstrap4/icons/all.css') }}" rel="stylesheet" />


</head>

<body>
	<div class="row col-12 col-sm-12 col-md-12" style="text-align: center; align-content: center;">
        <div class="container">
        	@yield('content')
        </div>
  	</div>
</body>
  

<!--footer section end-->
<!-- move top -->
<button onclick="topFunction()" id="movetop" class="bg-primary" title="Go to top">
  <span class="fa fa-angle-up"></span>
</button>
<script>
  // When the user scrolls down 20px from the top of the document, show the button
  window.onscroll = function () {
    scrollFunction()
  };

  function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      document.getElementById("movetop").style.display = "block";
    } else {
      document.getElementById("movetop").style.display = "none";
    }
  }

  // When the user clicks on the button, scroll to the top of the document
  function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }
</script>
<!-- /move top -->


<script src="{{ asset('bootstrap4/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('bootstrap4/js/jquery-1.10.2.min.js') }}"></script>

<!-- chart js --
<script src="{{ asset('bootstrap4/js/Chart.min.js') }}"></script>
<script src="{{ asset('bootstrap4/js/utils.js') }}"></script>
<!-- //chart js -->

<!-- Different scripts of charts.  Ex.Barchart, Linechart --
<script src="{{ asset('bootstrap4/js/bar.js') }}"></script>
<script src="{{ asset('bootstrap4/js/linechart.js') }}"></script>
<!-- //Different scripts of charts.  Ex.Barchart, Linechart -->


<script src="{{ asset('bootstrap4/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('bootstrap4/js/scripts.js') }}"></script>

<!-- close script -->
<script>
  var closebtns = document.getElementsByClassName("close-grid");
  var i;

  for (i = 0; i < closebtns.length; i++) {
    closebtns[i].addEventListener("click", function () {
      this.parentElement.style.display = 'none';
    });
  }
</script>
<!-- //close script -->

<!-- disable body scroll when navbar is in active -->
<script>
  $(function () {
    $('.sidebar-menu-collapsed').click(function () {
      $('body').toggleClass('noscroll');
    })
  });
</script>
<!-- disable body scroll when navbar is in active -->

 <!-- loading-gif Js -->
 <script src="{{ asset('bootstrap4/js/modernizr.js') }}"></script>
 <script>
     $(window).load(function () {
         // Animate loader off screen
         $(".se-pre-con").fadeOut("slow");;
     });
 </script>
 <!--// loading-gif Js -->

<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('bootstrap4/js/bootstrap.min.js') }}"></script>

<script src="{{ asset('bootstrap4/js/console-img.min.js') }}"></script>

  <script type="text/javascript" src="{{ asset('bootstrap4/js/select/select.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bootstrap4/js/select/select2.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bootstrap4/js/acciones.js') }}"></script>


</html>
