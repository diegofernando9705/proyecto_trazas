@foreach($productos as $producto)
<div class="container">
	 @include('custom.parameters')

	 {!! Form::open(['route' => array('productosRemisiones.update', $producto->id_producto), 'files' => true]) !!}

		@csrf
		@method('PUT')
	
		<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row">
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>(*) Nombre del producto:</b></label>
					<input type="text" class="form-control" placeholder="" name="nombre_producto" value="{{ $producto->descripcion_producto }}"  />
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>(*) Estado producto</b></label>
					<select class="form-control" name="estado" >
						@if($producto->estado_producto == '1')
							<option value="1" selected="">Activo</option>
							<option value="0">Inactivo</option>
						@else
							<option value="1">Activo</option>
							<option value="0" selected="">Inactivo</option>
						@endif
					</select>
				</div>
			</div>
		</div>

		<div class="botones col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="margin-top: 40px; margin-bottom: 20px;">
			<center>
				<button class="btn btn-danger" type="button" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-ban"></i> Cancelar actualizacion
				</button>
				<button class="btn btn-success" type="submit">
					<i class="fas fa-sync-alt"></i> Actualizar
				</button>
			</center>
		</div>

	{!! Form::close() !!}
</div>
@endforeach