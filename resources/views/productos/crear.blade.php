@extends('layouts.app')

@section('content')
<div class="content " >
    <div class="container-fluid" style="background-color: white; padding-top: 10px;">
        <div class="row">
            <div class="col-md-12">
              <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h3 class="card-title mt-0">Registrar un nuevo producto</h3>
                </div>

                <div class="card-body">
                    @include('custom.message')
                  <div class="table-responsive">
                    
                    <form action="{{ route('productosRemisiones.store') }}" method="POST">
                        @csrf
                        @method('POST')

                        <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                    <label style="padding-bottom: 5px;"><b>(*) Nombre producto:</b></label>
                                  <input type="text" class="form-control" placeholder="" name="nombre_producto" value="{{ old('primer_nombre') }}">
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                   <label style="padding-bottom: 5px;"><b>(*) Estado producto</b></label>
		                            <select class="form-control" name="estado">
		                                <option value="1">Activo</option>
		                                <option value="0">Inactivo</option>
		                            </select>
                                </div>
                            </div>
                        </div>

                        <div class="botones col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="margin-top: 40px; margin-bottom: 20px;">
                            <center>
                                <button class="btn btn-danger" type="button">
                                    Cancelar registro
                                </button>
                                <button class="btn btn-success" type="submit">
                                    Registrar producto
                                </button>
                            </center>
                        </div>

                    </form>
                  </div>
                </div>

              </div>
            </div>
        </div>
    </div>
</div>


@endsection
