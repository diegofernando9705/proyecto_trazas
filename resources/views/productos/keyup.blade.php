<thead>
                            <tr>
                                <th scope="col">Código producto</th>
                                <th scope="col"><b>Nombre producto</b></th>
                                <th scope="col"><b>Estado producto</b></th>
                                <th scope="col" style="text-align: center;" colspan="2"><b>Acciones</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                             @foreach($productos as $producto)
                             
                             <tr>
                                <td>{{ $producto->id_producto }}</td>
                                <td>{{ $producto->descripcion_producto }}</td>
                                @if($producto->estado_producto == '1')
									<td class="alert alert-success">Activo</td>
								@else
									<td class="alert alert-danger">Inactivo</td>
								@endif

                                <td style="text-align: center;">
                                    <button type="button" class="btn btn-success btnEdicion" title="Editar registro" data-id="{{ $producto->id_producto }}" data-url="{{ route('productosRemisiones.edit', $producto->id_producto ) }}"><i class="far fa-edit"></i></button>
                                </td>
                                <td>
                                    <center>
                                    @if($producto->estado_producto == '1')
										<form method="POST" action="{{ route('productosRemisiones.destroy', $producto->id_producto)}}">
	                                        @csrf
	                                        @method('DELETE')
	                                        <button type="submit" class="btn btn-danger" title="Eliminar registro">
	                                            <i class="fas fa-trash"></i>
	                                        </button>
                                    	</form>
									@else
										<button type="submit" class="btn btn-danger" title="Eliminar registro" disabled="">
                                            <i class="fas fa-trash"></i>
                                        </button>
									@endif
                                    
                                    </center>
                                </td>
                             </tr>
                             @endforeach
                        </tbody>