@extends('layouts.app')

@section('content')
<div class="content " >
    <div class="container-fluid" style="background-color: white; padding-top: 10px;">
        <div class="row">
            <div class="col-md-12">
              <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h3 class="card-title mt-0">Modulo de productos</h3>
                  <p class="card-category">Acá puede ver todos los productos registrados en el sistema</p>
                </div>
                <div class="card-body">
                    @include('custom.message')
                  <div class="table-responsive">
                    <div class="form-group">
                        <label><b>Búsqueda de Productos...</b></label>
                        <input type="text" class="form-control" id="busqueda_producto" name="busqueda_producto" placeholder="Digite descripcion o nombre del producto a buscar">
                    </div>
                    <hr style="margin-top: 10px; margin-bottom: 20px;">
                    <table class="table table-hover table-bordered" id="resultado_consulta">
                        <thead>
                            <tr>
                                <th scope="col">Código producto</th>
                                <th scope="col"><b>Nombre producto</b></th>
                                <th scope="col"><b>Estado producto</b></th>
                                <th scope="col" style="text-align: center;" colspan="2"><b>Acciones</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                             @foreach($productos as $producto)
                             
                             <tr>
                                <td>{{ $producto->id_producto }}</td>
                                <td>{{ $producto->descripcion_producto }}</td>
                                @if($producto->estado_producto == '1')
									<td class="alert alert-success">Activo</td>
								@else
									<td class="alert alert-danger">Inactivo</td>
								@endif

                                <td style="text-align: center;">
                                    <button type="button" class="btn btn-success btnEdicion" title="Editar registro" data-id="{{ $producto->id_producto }}" data-url="{{ route('productosRemisiones.edit', $producto->id_producto ) }}"><i class="far fa-edit"></i></button>
                                </td>
                                <td>
                                    <center>
                                    @if($producto->estado_producto == '1')
										<form method="POST" action="{{ route('productosRemisiones.destroy', $producto->id_producto)}}">
	                                        @csrf
	                                        @method('DELETE')
	                                        <button type="submit" class="btn btn-danger" title="Eliminar registro">
	                                            <i class="fas fa-trash"></i>
	                                        </button>
                                    	</form>
									@else
										<button type="submit" class="btn btn-danger" title="Eliminar registro" disabled="">
                                            <i class="fas fa-trash"></i>
                                        </button>
									@endif
                                    
                                    </center>
                                </td>
                             </tr>
                             @endforeach
                        </tbody>
                    </table>
                    {{ $productos->links() }}
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>


@endsection
