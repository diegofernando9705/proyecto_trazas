@foreach($conductores as $conductor)
<div class="container">
	 @include('custom.parameters')

	 {!! Form::open(['route' => array('conductoresRemisiones.update', $conductor->id), 'files' => true]) !!}

		@csrf
		@method('PUT')

		<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row">
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>(*) Tipo de identificación:</b></label>
					<select class="form-control" name="tipo_identificacion" readonly="">
						@foreach($tipo_identificacion as $tipo)
							@if($conductor->id_tipo_conductor == $tipo->id_tipo)
								<option value="{{ $tipo->id_tipo }}" selected="">{{ $tipo->descripcion_tipo }}</option>
							@else
								<option value="{{ $tipo->id_tipo }}">{{ $tipo->descripcion_tipo }}</option>
							@endif
						@endforeach
					</select>
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>(*) Número de identificación:</b></label>
					<input type="text" class="form-control" placeholder="" name="identificacion" value="{{ $conductor->identificacion_conductor }}"  readonly="" />
				</div>
			</div>
		</div>

		<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row">
				<?php 
					$array_nombre = explode(" ", $conductor->nombres_conductor);
				?>
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>(*) Primer nombre:</b></label>
					<input type="text" class="form-control" placeholder="" name="primer_nombre" value="{{ $array_nombre[0] }}" >
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>Segundo nombre:</b></label>
					<input type="text" class="form-control" placeholder="" name="segundo_nombre" value="{{ $array_nombre[1] }}" >
				</div>
			</div>
		</div>

		<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row">
				<?php 
					$array_apellido = explode(" ", $conductor->apellidos_conductor);
				?>
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>(*) Primer apellido</b></label>
					<input type="text" class="form-control" placeholder="" name="primer_apellido" value="{{ $array_apellido[0] }}" >
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b> Segundo apellido:</b></label>
					<input type="text" class="form-control" placeholder="" name="segundo_apellido" value="{{ $array_apellido[1] }}" >
				</div>
			</div>
		</div>

		<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row">
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>Celular</b></label>
					<input type="text" class="form-control" placeholder="" name="celular" value="{{ $conductor->celular_1_conductor }}" >
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>Celular opcional:</b></label>
					<input type="text" class="form-control" placeholder="" name="celular_opcional" value="{{ $conductor->celular_2_conductor }}" >
				</div>
			</div>
		</div>

		<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
			<label style="padding-bottom: 5px;"><b>(*) Placa vehiculo</b></label>
			<input type="text" class="form-control" placeholder="" name="placa_vehiculo" value="{{ $conductor->placa_conductor }}" >
		</div>
		
		<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
			<label style="padding-bottom: 5px;"><b>(*) Estado conductor</b></label>
			<select class="form-control" name="estado" >
				@if($conductor->estado_conductor == '1')
					<option value="1" selected="">Activo</option>
					<option value="0">Inactivo</option>
				@else
					<option value="1">Activo</option>
					<option value="0" selected="">Inactivo</option>
				@endif
			</select>
		</div>
		<div class="botones col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="margin-top: 40px; margin-bottom: 20px;">
			<center>
				<button class="btn btn-danger" type="button" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-ban"></i> Cancelar actualizacion
				</button>
				<button class="btn btn-success" type="submit">
					<i class="fas fa-sync-alt"></i> Actualizar
				</button>
			</center>
		</div>

	{!! Form::close() !!}

</div>
@endforeach