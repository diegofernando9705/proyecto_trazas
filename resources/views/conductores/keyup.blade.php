<thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col"><b>Identificacion</b></th>
                                <th scope="col"><b>Nombre</b></th>
                                <th scope="col"><b>Apellido</b></th>
                                <th scope="col"><b>Celular</b></th>
                                <th scope="col"><b>Estado</b></th>
                                <th scope="col" style="text-align: center;" colspan="2"><b>Acciones</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                             @foreach($conductores as $conductor)
                             
                             <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $conductor->identificacion_conductor }}</td>
                                <td>{{ $conductor->nombres_conductor }}</td>
                                <td>{{ $conductor->apellidos_conductor }}</td>
                                <td>{{ $conductor->celular_1_conductor }}</td>
                                @if($conductor->estado_conductor == '1')
                                    <td class="alert alert-success">Activo</td>
                                @else
                                    <td class="alert alert-danger">Inactivo</td>
                                @endif
                                <td style="text-align: center;">
                                    <button type="button" class="btn btn-info btnVer" title="Ver registro" data-id="{{ $conductor->id }}" data-url="{{ route('conductoresRemisiones.show', $conductor->id ) }}"><i class="fas fa-eye"></i></button>
                                    <button type="button" class="btn btn-success btnEdicion" title="Editar registro" data-id="{{ $conductor->id }}" data-url="{{ route('conductoresRemisiones.edit', $conductor->id ) }}"><i class="far fa-edit"></i></button>
                                </td>
                                <td>
                                    <center>
                                    <form method="POST" action="{{ route('conductoresRemisiones.destroy', $conductor->id)}}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger" title="Eliminar registro">
                                            <i class="fas fa-user-times"></i>
                                        </button>
                                    </form>
                                    </center>
                                </td>
                             </tr>
                             @endforeach
                        </tbody>