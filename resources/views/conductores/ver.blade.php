@foreach($conductores as $conductor)
<div class="container">
	 @include('custom.parameters')
	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>(*) Tipo de identificación:</b></label>
				<select class="form-control" name="tipo_identificacion" readonly="" disabled="">
					@foreach($tipo_identificacion as $tipo)
						@if($conductor->id_tipo_conductor == $tipo->id_tipo)
							<option value="{{ $tipo->id_tipo }}" selected="">{{ $tipo->descripcion_tipo }}</option>
						@else
							<option value="{{ $tipo->id_tipo }}">{{ $tipo->descripcion_tipo }}</option>
						@endif
					@endforeach
				</select>
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>(*) Número de identificación:</b></label>
				<input type="text" class="form-control" placeholder="" name="identificacion" value="{{ $conductor->identificacion_conductor }}" disabled="" />
			</div>
		</div>
	</div>

	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<?php 
				$array_nombre = explode(" ", $conductor->nombres_conductor);
			?>
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>(*) Primer nombre:</b></label>
				<input type="text" class="form-control" placeholder="" name="primer_nombre" value="{{ $array_nombre[0] }}" disabled="">
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>Segundo nombre:</b></label>
				<input type="text" class="form-control" placeholder="" name="segundo_nombre" value="{{ $array_nombre[1] }}" disabled="">
			</div>
		</div>
	</div>

	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<?php 
				$array_apellido = explode(" ", $conductor->apellidos_conductor);
			?>
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>(*) Primer apellido</b></label>
				<input type="text" class="form-control" placeholder="" name="primer_apellido" value="{{ $array_apellido[0] }}" disabled="">
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b> Segundo apellido:</b></label>
				<input type="text" class="form-control" placeholder="" name="segundo_apellido" value="{{ $array_apellido[1] }}" disabled="">
			</div>
		</div>
	</div>

	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>Celular</b></label>
				<input type="text" class="form-control" placeholder="" name="telefono_fijo" value="{{ $conductor->celular_1_conductor }}" disabled="">
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>Celular opcional:</b></label>
				<input type="text" class="form-control" placeholder="" name="celular" value="{{ $conductor->celular_2_conductor }}" disabled="">
			</div>
		</div>
	</div>

	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<label style="padding-bottom: 5px;"><b>(*) Placa vehiculo</b></label>
		<input type="email" class="form-control" placeholder="" name="correo" value="{{ $conductor->placa_conductor }}" disabled="">
	</div>
	
	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<label style="padding-bottom: 5px;"><b>(*) Estado conductor</b></label>
		<select class="form-control" name="estado" disabled="">
			@if($conductor->estado_conductor == '1')
				<option value="1" selected="">Activo</option>
				<option value="0">Inactivo</option>
			@else
				<option value="1">Activo</option>
				<option value="0" selected="">Inactivo</option>
			@endif
		</select>
	</div>

</div>
@endforeach