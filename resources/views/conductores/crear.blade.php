@extends('layouts.app')

@section('content')
<div class="content " >
    <div class="container-fluid" style="background-color: white; padding-top: 10px;">
        <div class="row">
            <div class="col-md-12">
              <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h3 class="card-title mt-0">Registrar un nuevo conductor</h3>
                </div>

                <div class="card-body">
                    @include('custom.message')
                  <div class="table-responsive">
                    
                    <form action="{{ route('conductoresRemisiones.store') }}" method="POST">
                        @csrf
                        @method('POST')

                        <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                    <label style="padding-bottom: 5px;"><b>(*) Tipo de identificación:</b></label>
                                    <select class="form-control" name="tipo_identificacion">
                                        <option value="" selected="">Seleccione un tipo de identificación</option>
                                        @foreach($tipo_identificacion as $tipo)
                                        <option value="{{ $tipo->id_tipo }}">{{ $tipo->descripcion_tipo }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                    <label style="padding-bottom: 5px;"><b>(*) Número de identificación:</b></label>
                                  <input type="text" class="form-control" placeholder="" name="identificacion" value="{{ old('identificacion') }}">
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                    <label style="padding-bottom: 5px;"><b>(*) Primer nombre:</b></label>
                                  <input type="text" class="form-control" placeholder="" name="primer_nombre" value="{{ old('primer_nombre') }}">
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                    <label style="padding-bottom: 5px;"><b>Segundo nombre:</b></label>
                                  <input type="text" class="form-control" placeholder="" name="segundo_nombre" value="{{ old('segundo_nombre') }}">
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                    <label style="padding-bottom: 5px;"><b>(*) Primer apellido</b></label>
                                  <input type="text" class="form-control" placeholder="" name="primer_apellido" value="{{ old('primer_apellido') }}">
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                    <label style="padding-bottom: 5px;"><b> Segundo apellido:</b></label>
                                  <input type="text" class="form-control" placeholder="" name="segundo_apellido" value="{{ old('segundo_apellido') }}">
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                    <label style="padding-bottom: 5px;"><b>(*) Celular</b></label>
                                  <input type="text" class="form-control" placeholder="" name="celular" value="{{ old('celular') }}">
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                    <label style="padding-bottom: 5px;"><b>Celular opcional:</b></label>
                                  <input type="text" class="form-control" placeholder="" name="celular_opcional" value="{{ old('celular_opcional') }}">
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                    <label style="padding-bottom: 5px;"><b>(*) Placa del vehiculo</b></label>
                                  <input type="text" class="form-control" placeholder="" name="placa_vehiculo" value="{{ old('placa_vehiculo') }}">
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                	<label style="padding-bottom: 5px;"><b>(*) Estado conductor</b></label>
		                            <select class="form-control" name="estado">
		                                <option value="1">Activo</option>
		                                <option value="0">Inactivo</option>
		                            </select>
                                </div>
                            </div>
                        </div>

                        <div class="botones col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="margin-top: 40px; margin-bottom: 20px;">
                            <center>
                                <button class="btn btn-danger" type="button">
                                    Cancelar registro
                                </button>
                                <button class="btn btn-success" type="submit">
                                    Registrar conductor
                                </button>
                            </center>
                        </div>

                    </form>
                  </div>
                </div>

              </div>
            </div>
        </div>
    </div>
</div>


@endsection
