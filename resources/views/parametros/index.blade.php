@extends('layouts.app')

@section('content')
<div class="content " >
    <div class="container-fluid" style="background-color: white; padding-top: 10px;">
        <div class="row">
            <div class="col-md-12">
              <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h3 class="card-title mt-0">Par&aacute;metros de la Compañ&iacute;a</h3>
                  <p class="card-category">Acá puede ver todos los parametroes registrados en el sistema</p>
                </div>
                <div class="card-body">
                    @include('custom.message')
                  <div class="table-responsive">
                    <!--<div class="form-group">
                        <label><b>Búsqueda de parametros...</b></label>
                        <input type="text" class="form-control" id="busqueda_parametros" name="busqueda_parametros" placeholder="Digite nombre, apellido o placa del parametro a buscar">
                    </div>-->
                    <hr style="margin-top: 10px; margin-bottom: 20px;">
                    <table class="table table-hover table-bordered" id="resultado_consulta">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col"><b>Identificacion</b></th>
                                <th scope="col"><b>Nombre</b></th>
                                <th scope="col"><b>Apellido</b></th>
                                <th scope="col"><b>Celular</b></th>
                                <th scope="col"><b>Email</b></th>
                                <th scope="col"><b>Estado</b></th>
                                <th scope="col" style="text-align: center;" colspan="2"><b>Acciones</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                             @foreach($parametros as $parametro)
                             
                             <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $parametro->identificacion_parametros }}</td>
                                <td>{{ $parametro->nombre_parametros }}</td>
                                <td>{{ $parametro->direccion_parametros }}</td>
                                <td>{{ $parametro->celular_1_parametros }}</td>
                                <td>{{ $parametro->email_cliente }}</td>
                                @if($parametro->estado_parametro == '1')
                                    <td class="alert alert-success">Activo</td>
                                @else
                                    <td class="alert alert-danger">Inactivo</td>
                                @endif
                                <td style="text-align: center;">
                                    <button type="button" class="btn btn-info btnVer" title="Ver registro" data-id="{{ $parametro->id }}" data-url="{{ route('parametrosRemisiones.show', $parametro->id ) }}"><i class="fas fa-eye"></i></button>
                                    <button type="button" class="btn btn-success btnEdicion" title="Editar registro" data-id="{{ $parametro->id }}" data-url="{{ route('parametrosRemisiones.edit', $parametro->id ) }}"><i class="far fa-edit"></i></button>
                                </td>
                                <td>
                                    <center>
                                    <form method="POST" action="{{ route('parametrosRemisiones.destroy', $parametro->id)}}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger" title="Eliminar registro">
                                            <i class="fas fa-user-times"></i>
                                        </button>
                                    </form>
                                    </center>
                                </td>
                             </tr>
                             @endforeach
                        </tbody>
                    </table>
                    {{ $parametros->links() }}
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>


@endsection
