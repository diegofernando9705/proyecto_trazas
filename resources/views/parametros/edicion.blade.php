@foreach($parametros as $parametro)
<div class="container">
	 @include('custom.parameters')

	 {!! Form::open(['route' => array('parametrosRemisiones.update', $parametro->id), 'files' => true]) !!}

		@csrf
		@method('PUT')

		<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row">
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>(*) Tipo de identificación:</b></label>
					<select class="form-control" name="tipo_identificacion" readonly="">
						<option value="" selected="">Seleccione un tipo de identificación</option>
						@foreach($tipo_identificacion as $tipo)
							@if($parametro->id_tipo_parametro == $tipo->id_tipo)
								<option value="{{ $tipo->id_tipo }}" selected="">{{ $tipo->descripcion_tipo }}</option>
							@else
								<option value="{{ $tipo->id_tipo }}">{{ $tipo->descripcion_tipo }}</option>
							@endif
						@endforeach
					</select>
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>(*) Número de identificación:</b></label>
					<input type="text" class="form-control" placeholder="" name="identificacion" value="{{ $parametro->identificacion_parametros }}" readonly="">
				</div>
			</div>
		</div>

		<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row">
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>(*) Logo compañ&iacute;a:</b></label>
					<input type="hidden" name="imagen_bd" value="{{ $parametro->logo_parametros }}">
					<img src="{{ asset('storage/'.$parametro->logo_parametros) }}" width="100%">
					<input type="file" name="logo">
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>(*) Raz&oacute;n social:</b></label>
					<input type="text" class="form-control" placeholder="" name="razon_social" value="{{ $parametro->nombre_parametros }}" >
				</div>
			</div>
		</div>

		<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row">
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>(*) Direcci&oacute;n  compañ&iacute;a:</b></label>
					<input type="text" class="form-control" placeholder="" name="direccion" name="direccion" value="{{ $parametro->direccion_parametros }}" >
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>(*) Ciudad compañ&iacute;a:</b></label>
					<input type="text" class="form-control" placeholder="" name="ciudad" value="{{ $parametro->id_ciudad_parametros }}" >
				</div>
			</div>
		</div>

		<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row">
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>Tel&eacute;fono fijo:</b></label>
					<input type="text" class="form-control" placeholder=""  name="telefono_fijo" value="{{ $parametro->telefono_fijo_parametros }}" >
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>(*) Celular:</b></label>
					<input type="text" class="form-control" placeholder="" name="celular" value="{{ $parametro->celular_1_parametros }}" >
				</div>
			</div>
		</div>

		<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row">
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>Celular 1:</b></label>
					<input type="text" class="form-control" placeholder=""  name="celular_1" value="{{ $parametro->celular_2_parametros }}" >
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>Celular 2:</b></label>
					<input type="text" class="form-control" placeholder=""  name="celular_2" value="{{ $parametro->celular_3_parametros }}" >
				</div>
			</div>
		</div>

		<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row">
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>(*) Correo electr&oacute;nico:</b></label>
					<input type="text" class="form-control" placeholder="" name="email" value="{{ $parametro->email_cliente }}" >
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>(*) Estado par&aacute;metro:</b></label>
					<select class="form-control"  name="estado">
						@if($parametro->estado_parametro == '1')
							<option value="1" selected="">Activo</option>
							<option value="0">Inactivo</option>
						@else
							<option value="1">Activo</option>
							<option value="0" selected="">Inactivo</option>
						@endif
						
					</select>
				</div>
			</div>
		</div>

		<div class="botones col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="margin-top: 40px; margin-bottom: 20px;">
			<center>
				<button class="btn btn-danger" type="button" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-ban"></i> Cancelar actualizacion
				</button>
				<button class="btn btn-success" type="submit">
					<i class="fas fa-sync-alt"></i> Actualizar
				</button>
			</center>
		</div>

	{!! Form::close() !!}
</div>
@endforeach