@extends('layouts.app')

@section('content')
<div class="content " >
    <div class="container-fluid" style="background-color: white; padding-top: 10px;">
        <div class="row">
            <div class="col-md-12">
              <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h3 class="card-title mt-0">Modulo de usuarios</h3>
                  <p class="card-category">Acá puede ver todos los usuarios registrados en el sistema</p>
                </div>
                <div class="card-body">
                    @include('custom.message')
                  <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col"><b>Nombre</b></th>
                                <th scope="col"><b>Correo</b></th>
                                <th scope="col"><b>Rol(es)</b></th>
                                <th colspan="3" style="text-align: center;"><b>Acciones</b></th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($users as $user)

                            <tr>
                                <th scope="row">{{ $user->id}}</th>
                                <td>{{ $user->name}}</td>
                                <td>{{ $user->email}}</td>
                                <td>
                                    @isset( $user->roles[0]->name )
                                    {{ $user->roles[0]->name}}
                                    @endisset

                                </td>
                                <td> 
                                    
                                </td>  
                                <td> 
                                    @can('view', [$user, ['user.edit','userown.edit'] ])
                                    <a class="btn btn-success" href="{{ route('user.edit',$user->id)}}">
                                        <i class="material-icons">create</i>
                                    </a> 
                                    @endcan
                                </td>  
                                <td> 
                                    @can('haveaccess','user.destroy')
                                    <form action="{{ route('user.destroy',$user->id)}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger">
                                            <i class="material-icons">delete_forever</i>
                                        </button>
                                    </form>
                                    @endcan
                                </td>  
                            </tr>      
                            @endforeach
                        </tbody>
                    </table>
                    {{ $users->links() }}

                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>


@endsection
