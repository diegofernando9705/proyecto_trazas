@extends('layouts.app')

@section('content')

<div class="content " >
    <div class="container-fluid" style="background-color: white; padding-top: 10px;">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
              <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h2 class="card-title mt-0">Edicion de usuario</h2>
                  <!--<p class="card-category">Acá puede ver todos los usuarios registrados en el sistema</p>-->
                </div>
                <div class="card-body">
                    @include('custom.message')
                  <div class="table-responsive">
                    <form action="{{ route('user.update', $user->id)}}" method="POST">
                      @csrf
                      @method('PUT')
                      <div class="form-group">
                        <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="{{ old('name', $user->name)}}" />
                      </div>
                      <div class="form-group">                            
                        <input type="text" class="form-control" id="email" placeholder="email" name="email" value="{{ old('email' , $user->email)}}" />
                      </div>
                      <div class="form-group">
                        <select  class="form-control"  name="roles" id="roles">
                          @foreach($roles as $role)
                            <option value="{{ $role->id }}"
                              @isset($user->roles[0]->name)
                              @if($role->name ==  $user->roles[0]->name)
                                selected
                              @endif
                              @endisset
                              >
                              {{ $role->name }}
                            </option>
                          @endforeach
                        </select>
                      </div>

                      <br><br>
                      <center>

                        <a href="{{route('user.index')}}">
                          <button class="btn btn-danger" type="button">
                            <i class="material-icons">keyboard_backspace</i> Regresar
                          </button>
                        </a>

                        <button class="btn btn-primary" type="submit">
                          <i class="material-icons">update</i> Actualizar
                        </button>
                        
                      </center>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

@endsection
