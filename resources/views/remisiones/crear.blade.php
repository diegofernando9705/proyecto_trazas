@extends('layouts.app')

@section('content')
<div class="content " >
    <div class="container-fluid" style="background-color: white; padding-top: 10px;">
        <div class="row">
            <div class="col-md-12">
              <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h3 class="card-title mt-0">Crear registro</h3>
                </div>

                <div class="card-body">
                	<div class="alert alert-danger">
                		<b>TODOS</b> los campos marcados con asteriscos (*) son obligatorios.
                	</div>
                    @include('custom.message')
                  	<div class="table-responsive">
                  		<form action="{{ route('remisiones.store') }}" method="POST">
                        @csrf
                        @method('POST')

                        <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
                        	@foreach($parametros as $parametro)
	                        	<div class="row">
	                                <div class="col-12 col-sm-9 col-md-9 col-lg-9" style="text-align: center;">
	                                   <p>
	                                   		{{ $parametro->nombre_parametros }} <br>
	                                   		@foreach($tipo_identificacion as $tipo)
	                                   			@if($parametro->id_tipo_parametro == $tipo->id_tipo)
	                                   				{{ $tipo->descripcion_tipo }}
	                                   			@endif
	                                   		@endforeach
	                                   		{{ $parametro->identificacion_parametros }} R&eacute;gimen Com&uacuten; <br>
	                                   		{{ $parametro->direccion_parametros }} <br>
	                                   		Cel: {{ $parametro->telefono_fijo_parametros }} - {{ $parametro->celular_1_parametros }} - {{ $parametro->celular_2_parametros }} - {{ $parametro->celular_3_parametros }} <br>
	                                   		Email: {{ $parametro->email_cliente }} <br>
	                                   		{{ $parametro->id_ciudad_parametros }}
	                                   </p>
	                                </div>
	                                <div class="col-12 col-sm-3 col-md-3 col-lg-3">
	                                   <img src="{{ asset('storage/'.$parametro->logo_parametros) }}">
	                                </div>
	                            </div>
                        	@endforeach
                        </div>

                        <!-- NUMERO DE REMISION -->
                        <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
                        	<div class="row">
                        		<div class="col-12 col-sm-9 col-md-9 col-lg-9" style="text-align: center;"></div>
                        		<div class="col-12 col-sm-3 col-md-3 col-lg-3">
                        			<div class="form-group row">
                        				<label for="staticEmail" class="col-sm-5 col-form-label">
                        					<b>(*) REMISION No.</b>
                        				</label>
                        				<div class="col-sm-7">
                        					<input type="number" name="codigo_remision" class="form-control" id="staticEmail" value="{{ $numero_remision }}" />
										</div>
									</div>
								</div>
							</div>
                        </div>

                        <!-- CLIENTE Y FECHA -->
                        <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
                        	<div class="row">
                        		<div class="col-12 col-sm-7 col-md-7 col-lg-7" style="padding-top:30px; border: 1px solid black; border-radius: 15px;">
                        			<div class="form-group row">
                        				<label for="staticEmail" class="col-sm-2 col-form-label">
                        					<b>(*) Nombre</b>
                        				</label>
                        				<div class="col-sm-10">
                        					<select class="form-control mi-selector col-12 col-sm-12 col-md-12 col-lg-12" name="identificacion_cliente">
                        							(*) <option value="0">Selecciona un cliente</option>
	                        					@foreach($clientes as $cliente)
	                        						<option value="{{ $cliente->identificacion }}">{{ $cliente->identificacion }} - {{ $cliente->nombres_cliente }} {{ $cliente->apellidos_cliente }}</option>
	                        					@endforeach                        						
                        					</select>
										</div>
									</div>
									<div class="form-group row">
                        				<label for="staticEmail" class="col-sm-2 col-form-label">
                        					<b>(*) Nit. / C.C</b>
                        				</label>
                        				<div class="col-sm-10">
                        					<input type="text" class="form-control-plaintext" id="identificacion_formulario" />
										</div>
									</div>
									<div class="form-group row">
                        				<label for="staticEmail" class="col-sm-2 col-form-label">
                        					<b>(*) Direccion:</b>
                        				</label>
                        				<div class="col-sm-10">
                        					<input type="text" class="form-control" id="direccion_formulario" />
										</div>
									</div>
									<div class="form-group row">
                        				<label for="staticEmail" class="col-sm-2 col-form-label">
                        					<b>(*) Ciudad:</b>
                        				</label>
                        				<div class="col-sm-10">
                        					<input type="text" class="form-control-plaintext" id="ciudad_formulario" />
										</div>
									</div>
									<div class="form-group row">
                        				<label for="staticEmail" class="col-sm-2 col-form-label">
                        					<b>(*) Telefono:</b>
                        				</label>
                        				<div class="col-sm-10">
                        					<input type="text" class="form-control" id="telefono_formulario" />
										</div>
									</div>
									<div class="form-group row">
                        				<label for="staticEmail" class="col-sm-2 col-form-label">
                        					<b>Email:</b>
                        				</label>
                        				<div class="col-sm-10">
                        					<input type="email" class="form-control" id="email_formulario" />
										</div>
									</div>
                        		</div>
                        		<div class="col-12 col-sm-4 col-md-4 col-lg-4" style="margin-left:20px; padding-top:30px; text-align: center; border: 1px solid black; border-radius: 15px; ">
                        			<div class="form-group row">
                        				<label for="staticEmail" class="col-sm-3 col-form-label">
                        					<b>(*) FECHA</b>
                        				</label>
                        				<div class="col-sm-9">
                        					<input type="date" class="form-control" name="fecha_remision" value="<?php echo date('Y-m-d'); ?>"/>
										</div>
									</div>
									<div class="form-group row">
                        				<label for="staticEmail" class="col-sm-2 col-form-label">
                        					<b>Vence</b>
                        				</label>
                        				<div class="col-sm-10">
                        					<input type="text" class="form-control-plaintext" name="vence_remision" />
										</div>
									</div>
									<div class="form-group row">
                        				<label for="staticEmail" class="col-sm-3 col-form-label">
                        					<b>(*) Orden:</b>
                        				</label>
                        				<div class="col-sm-9">
                        					<input type="text" class="form-control" name="orden_remision" />
										</div>
									</div>
								</div>
							</div>
                        </div>

                        <!-- PRODUCTOS -->
                        <div class="form-group col-12 col-sm-11 col-md-11 col-lg-11">
                        	<div class="row">
                        		<div class="col-12 col-sm-12 col-md-12 col-lg-12" style="padding-top:30px; border: 1px solid black; border-radius: 15px;">
	                        		<table id="lista_productos" class="table table-bordered">
	                        			<thead>
	                        				<tr>
	                        					<th>(*) C&oacute;digo</th>
	                        					<th>(*) Descripci&oacute;n</th>
	                        					<th>(*) Certificado de origen</th>
	                        					<th>Cantidad</th>
	                        					<th>Total</th>
	                        				</tr>
	                        			</thead>

	                        			<tbody>
	                        				<tr>
	                        					<td>
	                        						<select class="form-control mi-selector-tabla col-12 col-sm-12 col-md-12 col-lg-12" name="codigo_producto[]" data-id="1" id="codigo_1">
                        								<option value="0">Selecciona un c&oacute;digo de producto</option>
				                        				@foreach($productos as $producto)
				                        					<option value="{{ $producto->id_producto }}">{{ $producto->id_producto }}</option>
				                        				@endforeach
                        							</select>
	                        					</td>
	                        					<td>
	                        						<select class="form-control mi-selector-tabla-producto col-12 col-sm-12 col-md-12 col-lg-12" id="1" data-id="1">
                        								<option value="0">Selecciona un producto</option>
				                        				@foreach($productos as $producto)
				                        					<option value="{{ $producto->id_producto }}">{{ $producto->descripcion_producto }}</option>
				                        				@endforeach                        						
                        							</select>
	                        					</td>
	                        					<td>
	                        						<input type="text" class="form-control" name="certificado_origen[]" required="">
	                        					</td>
	                        					<td>
	                        						<input type="number" class="form-control" name="cantidad_producto[]">
	                        					</td>
	                        					<td>
	                        						<input type="text" class="form-control" name="total_producto[]">
	                        					</td>
	                        					<!--<td>
	                        						<button class="btn btn-danger button_eliminar_producto">eliminar</button>
	                        					</td>-->
	                        				</tr>
											<tr>
	                        					<td>
	                        						<select class="form-control mi-selector-tabla col-12 col-sm-12 col-md-12 col-lg-12" name="codigo_producto[]" data-id="2" id="codigo_2">
                        								<option value="0">Selecciona un c&oacute;digo de producto</option>
				                        				@foreach($productos as $producto)
				                        					<option value="{{ $producto->id_producto }}">{{ $producto->id_producto }}</option>
				                        				@endforeach                        						
                        							</select>
	                        					</td>
	                        					<td>
	                        						<select class="form-control mi-selector-tabla-producto col-12 col-sm-12 col-md-12 col-lg-12" id="2" data-id="2">
                        								<option value="0">Selecciona un producto</option>
				                        				@foreach($productos as $producto)
				                        					<option value="{{ $producto->id_producto }}">{{ $producto->descripcion_producto }}</option>
				                        				@endforeach                        						
                        							</select>
	                        					</td>
	                        					<td>
	                        						<input type="text" class="form-control" name="certificado_origen[]">
	                        					</td>
	                        					<td>
	                        						<input type="number" class="form-control" name="cantidad_producto[]">
	                        					</td>
	                        					<td>
	                        						<input type="text" class="form-control" name="total_producto[]">
	                        					</td>
	                        					<td>
	                        						<!--<button class="btn btn-danger button_eliminar_producto">eliminar</button>-->
	                        					</td>
	                        				</tr>
	                        				<tr>
	                        					<td>
	                        						<select class="form-control mi-selector-tabla col-12 col-sm-12 col-md-12 col-lg-12" name="codigo_producto[]" data-id="3" id="codigo_3">
                        								<option value="0">Selecciona un c&oacute;digo de producto</option>
				                        				@foreach($productos as $producto)
				                        					<option value="{{ $producto->id_producto }}">{{ $producto->id_producto }}</option>
				                        				@endforeach                        						
                        							</select>
	                        					</td>
	                        					<td>
	                        						<select class="form-control  mi-selector-tabla-producto col-12 col-sm-12 col-md-12 col-lg-12" id="3" data-id="3">
                        								<option value="0">Selecciona un producto</option>
				                        				@foreach($productos as $producto)
				                        					<option value="{{ $producto->id_producto }}">{{ $producto->descripcion_producto }}</option>
				                        				@endforeach                        						
                        							</select>
	                        					</td>
	                        					<td>
	                        						<input type="text" class="form-control" name="certificado_origen[]">
	                        					</td>
	                        					<td>
	                        						<input type="number" class="form-control" name="cantidad_producto[]">
	                        					</td>
	                        					<td>
	                        						<input type="text" class="form-control" name="total_producto[]">
	                        					</td>
	                        					<td>
	                        						<!--<button class="btn btn-danger button_eliminar_producto">eliminar</button>-->
	                        					</td>
	                        				</tr>
	                        				<tr>
	                        					<td>
	                        						<select class="form-control mi-selector-tabla col-12 col-sm-12 col-md-12 col-lg-12" name="codigo_producto[]" data-id="4" id="codigo_4">
                        								<option value="0">Selecciona un c&oacute;digo de producto</option>
				                        				@foreach($productos as $producto)
				                        					<option value="{{ $producto->id_producto }}">{{ $producto->id_producto }}</option>
				                        				@endforeach                        						
                        							</select>
	                        					</td>
	                        					<td>
	                        						<select class="form-control  mi-selector-tabla-producto col-12 col-sm-12 col-md-12 col-lg-12" id="4" data-id="4">
                        								<option value="0">Selecciona un producto</option>
				                        				@foreach($productos as $producto)
				                        					<option value="{{ $producto->id_producto }}">{{ $producto->descripcion_producto }}</option>
				                        				@endforeach                        						
                        							</select>
	                        					</td>
	                        					<td>
	                        						<input type="text" class="form-control" name="certificado_origen[]">
	                        					</td>
	                        					<td>
	                        						<input type="number" class="form-control" name="cantidad_producto[]">
	                        					</td>
	                        					<td>
	                        						<input type="text" class="form-control" name="total_producto[]">
	                        					</td>
	                        					<td>
	                        						<!--<button class="btn btn-danger button_eliminar_producto">eliminar</button>-->
	                        					</td>
	                        				</tr>
	                        				<tr>
	                        					<td>
	                        						<select class="form-control mi-selector-tabla col-12 col-sm-12 col-md-12 col-lg-12" name="codigo_producto[]" data-id="5" id="codigo_5">
                        								<option value="0">Selecciona un c&oacute;digo de producto</option>
				                        				@foreach($productos as $producto)
				                        					<option value="{{ $producto->id_producto }}">{{ $producto->id_producto }}</option>
				                        				@endforeach                        						
                        							</select>
	                        					</td>
	                        					<td>
	                        						<select class="form-control mi-selector-tabla-producto col-12 col-sm-12 col-md-12 col-lg-12" id="5" data-id="5">
                        								<option value="0">Selecciona un producto</option>
				                        				@foreach($productos as $producto)
				                        					<option value="{{ $producto->id_producto }}">{{ $producto->descripcion_producto }}</option>
				                        				@endforeach                        						
                        							</select>
	                        					</td>
	                        					<td>
	                        						<input type="text" class="form-control" name="certificado_origen[]">
	                        					</td>
	                        					<td>
	                        						<input type="number" class="form-control" name="cantidad_producto[]">
	                        					</td>
	                        					<td>
	                        						<input type="text" class="form-control" name="total_producto[]">
	                        					</td>
	                        					<!--<td>
	                        						<button class="btn btn-danger button_eliminar_producto">eliminar</button>
	                        					</td>-->
	                        				</tr>
	                        			</tbody>
	                        		</table>
	                        		<!--<button type="button" class="btn btn-info float-right button_agregar_producto"> Agregar producto</button><br><br>-->
                        		</div>
							</div>
                        </div>

						<!-- VALORES -->
                        <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
                        	<div class="row">
                        		<div class="col-12 col-sm-7 col-md-7 col-lg-7" style="padding-top:30px; border: 1px solid black; border-radius: 15px;">
                        			<div class="form-group row">
                        				<label for="staticEmail" class="col-sm-2 col-form-label">
                        					<b>Son:</b>
                        				</label>
                        				<div class="col-sm-10">
                        					<input type="text" class="form-control" name="son_remision">
										</div>
									</div>
									<div class="form-group row">
                        				<label for="staticEmail" class="col-sm-2 col-form-label">
                        					<b>Comentarios</b>
                        				</label>
                        				<div class="col-sm-10">
                        					<textarea class="form-control" name="comentarios_remision"></textarea>
										</div>
									</div>
                        		</div>
                        		<div class="col-12 col-sm-4 col-md-4 col-lg-4" style="margin-left:20px; padding-top:30px; text-align: center; border: 1px solid black; border-radius: 15px; ">
                        			<div class="form-group row">
                        				<label for="staticEmail" class="col-sm-2 col-form-label">
                        					<b>Subtotal</b>
                        				</label>
                        				<div class="col-sm-10">
                        					<input type="text" class="form-control" name="subtotal_remision" />
										</div>
									</div>
									<div class="form-group row">
                        				<label for="staticEmail" class="col-sm-2 col-form-label">
                        					<b>TOTAL</b>
                        				</label>
                        				<div class="col-sm-10">
                        					<input type="text" class="form-control" name="total_remision" />
										</div>
									</div>
								</div>
							</div>
                        </div>


                        <!-- CONDUCTOR -->
                        <div class="form-group col-12 col-sm-11 col-md-11 col-lg-11">
                        	<div class="row">
                        		<div class="col-12 col-sm-12 col-md-12 col-lg-12" style="padding-top:30px; border: 1px solid black; border-radius: 15px;">
                        			<h4><b>Conductor</b></h4>
                        			<br>
	                        		<table id="lista_productos" class="table table-bordered">
	                        			<thead>
	                        				<tr>
	                        					<th>(*) Placa del vehiculo</th>
	                        					<th>(*) Identificacion del conductor</th>
	                        					<th>(*) Nombre del conductor</th>
	                        					<th>(*) Celular conductor</th>
	                        				</tr>
	                        			</thead>

	                        			<tbody>
	                        				<tr>
	                        					<td>
	                        						<select class="form-control mi-selector col-12 col-sm-12 col-md-12 col-lg-12" data-informacion="placa" data-informacion="placa" id="placa_conductor">
                        								<option value="0">Selecciona una placa</option>
				                        				@foreach($conductores as $conductor)
				                        					<option value="{{ $conductor->placa_conductor }}">{{ $conductor->placa_conductor }}</option>
				                        				@endforeach                        						
                        							</select>
	                        					</td>
	                        					<td>
	                        						<select class="form-control mi-selector col-12 col-sm-12 col-md-12 col-lg-12" name="identificacion_conductor" data-informacion="identificacion" id="identificacion_conductor">
                        								<option value="0">Seleccionar por identificaci&oacute;n</option>
				                        				@foreach($conductores as $conductor)
				                        					<option value="{{ $conductor->identificacion_conductor }}">{{ $conductor->identificacion_conductor }}</option>
				                        				@endforeach                        						
                        							</select>
	                        					</td>
	                        					<td>
	                        						<select class="form-control mi-selector col-12 col-sm-12 col-md-12 col-lg-12" name="nombres_conductor[]" data-informacion="nombres" id="nombre_conductor">
                        								<option value="0">Seleccionar por nombre</option>
				                        				@foreach($conductores as $conductor)
				                        					<option value="{{ $conductor->identificacion_conductor }}">{{ $conductor->nombres_conductor }} {{ $conductor->apellidos_conductor }}</option>
				                        				@endforeach                        						
                        							</select>
	                        					</td>
	                        					<td>
	                        						<select class="form-control mi-selector col-12 col-sm-12 col-md-12 col-lg-12" data-informacion="celular" id="celular_conductor">
                        								<option value="0">Selecciona un producto</option>
				                        				@foreach($conductores as $conductor)
				                        					<option value="{{ $conductor->identificacion_conductor }}">{{ $conductor->celular_1_conductor }} - {{ $conductor->celular_2_conductor }}</option>
				                        				@endforeach                        						
                        							</select>
	                        					</td>
	                        					<!--<td>
	                        						<button class="btn btn-danger button_eliminar_producto">eliminar</button>
	                        					</td>-->
	                        				</tr>
	                        			</tbody>
	                        		</table>
	                        		<!--<button type="button" class="btn btn-info float-right button_agregar_producto"> Agregar producto</button><br><br>-->
                        		</div>
							</div>
                        </div>

                        <div class="botones col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="margin-top: 40px; margin-bottom: 20px;">
                            <center>
                            	<a href="{{ route('remisiones.index') }}">
                                <button class="btn btn-danger" type="button">
                                    Cancelar registro
                                </button>
                            	</a>
                                <button class="btn btn-success" type="submit">
                                    Registrar producto
                                </button>
                            </center>
                        </div>
                    	</form>
                 	</div>
                </div>

              </div>
            </div>
        </div>
    </div>
</div>


@endsection
