<!DOCTYPE html>
<html>
<head>
	<title>Remisiones del Sistema</title>
	<style type="text/css">
		.tabla{
			width: 100%;
		}
	</style>
</head>
<body>
	<div class="contenido" style="width: 100%;">
		<div style="width: 60%; float:left; border: 1px solid #9a9a9a; border-radius: 8px; padding-left: 20px; padding-top: 5px; padding-bottom: 5px; text-align: center; font-family: 'Nunito', sans-serif; font-size: 11px;">
			@foreach($parametros as $parametro)
			<p>
				<b>{{ $parametro->nombre_parametros }}</b> <br>
				Nit. {{ $parametro->identificacion_parametros }} R&eacute;gimen Co&uacute;m <br>
				{{ $parametro->direccion_parametros }} <br>
				Cel: {{ $parametro->telefono_fijo_parametros }} - {{ $parametro->celular_1_parametros }} - {{ $parametro->celular_2_parametros }} - {{ $parametro->celular_3_parametros }} <br>
				Email: {{ $parametro->email_cliente }} <br>
				{{ $parametro->id_ciudad_parametros }}
			</p>
			@endforeach
		</div>
		<div style="text-align:center; width: 30%; float: right; padding-left: 20px; padding-top: 5px; padding-bottom: 5px; font-family: 'Nunito', sans-serif; font-size:14px;">
			@foreach($parametros as $parametro)
				<img src="http://trazascol.softworldcolombia.com/img/icon-trazas.png" style="width: 100px;">
			@endforeach
		</div>
	</div>
	<div style="clear: both"></div>
	<div class="contenido" style="width: 100%;">
		@foreach($remision_unica as $remision_uni)
			<table class="tabla" style="width: 100%;">
				<thead>
					<tr>
						<th style="text-align: right; font-family: 'Nunito', sans-serif; font-size:14px;">REMISION No.</th>
						<td colspan="3" style="border: 1px solid #9a9a9a; font-family: 'Nunito', sans-serif; font-size:14px;">{{ $remision_uni->id_consecutivo }}</td>
					</tr>
				</thead>
			</table>
			<table style="width: 65%; float: left; border: 1px solid #9a9a9a; border-radius: 8px; padding-left: 20px; padding-top: 5px; padding-bottom: 5px; font-family: 'Nunito', sans-serif; font-size:14px;">
				<thead>
					<tr>
						<th style="text-align: left;">Nombre: </th>
						<td>{{ $remision_uni->nombres_cliente }}</td>
					</tr>
					<tr>
						<th style="text-align: left;">NIT: </th>
						<td>{{ $remision_uni->identificacion }}</td>
					</tr>
					<tr>
						<th style="text-align: left;">Direcci&oacute;n: </th>
						<td>{{ $remision_uni->direccion_cliente }}</td>
					</tr>
					<tr>
						<th style="text-align: left;">Ciudad: </th>
						<td>{{ $remision_uni->ciudad_cliente }}</td>
					</tr>
					<tr>
						<th style="text-align: left;">Telefono: </th>
						<td>{{ $remision_uni->telefono_fijo_cliente }} - {{ $remision_uni->celular_1_cliente }} - {{ $remision_uni->celular_2_cliente }}</td>
					</tr>
					<tr>
						<th style="text-align: left;">Email: </th>
						<td>{{ $remision_uni->email_cliente }}</td>
					</tr>
				</thead>
			</table>
			<table style="width: 33%; float: right; margin-left: 2%; border: 1px solid #9a9a9a; border-radius: 8px; padding-left: 20px; padding-top: 5px; padding-bottom: 5px; font-family: 'Nunito', sans-serif; font-size:14px; height: 50px;">
				<thead>
					<tr>
						<th style="text-align: left;">FECHA: </th>
						<td>{{ $remision_uni->fecha }}</td>
					</tr>
					<tr>
						<th style="text-align: left;">Vence: </th>
						<td >{{ $remision_uni->vence }}</td>
					</tr>
					<tr>
						<th style="text-align: left;">Orden: </th>
						<td>{{ $remision_uni->orden }}</td>
					</tr>
					<tr>
						<th>&nbsp;</th>
					</tr>
					<tr>
						<th>&nbsp;</th>
					</tr>
					<tr>
						<th>&nbsp;</th>
					</tr>
				</thead>
			</table>
		@endforeach
		<p style="color: white;">salto de linea</p>
		<table style="width: 100%;  border: 1px solid #9a9a9a; border-radius: 8px; padding-top: 5px; padding-bottom: 5px; margin-top: -30px; font-family: 'Nunito', sans-serif; font-size:14px;">
			<thead>
				<tr style="border: 1px solid #9a9a9a; padding-bottom: 5px;">
					<th style="border-bottom: 1px solid #9a9a9a; padding-bottom: 5px;">C&oacute;digo</th>
					<th style="border-bottom: 1px solid #9a9a9a; padding-bottom: 5px;">Descripci&oacute;n</th>
					<th style="border-bottom: 1px solid #9a9a9a; padding-bottom: 5px;">Certificado de origen</th>
					<th style="border-bottom: 1px solid #9a9a9a; padding-bottom: 5px;">Cantidad</th>
					<th style="border-bottom: 1px solid #9a9a9a; padding-bottom: 5px;">Total</th>
				</tr>
			</thead>
			<tbody>
				@foreach($remisiones as $remision)
				<tr>
					<td style="border-right:1px solid #9a9a9a; text-align: right; padding-right: 20px;">
						{{ $remision->id_producto }}
					</td>
					<td style="border-right:1px solid #9a9a9a; padding-left: 10px;">
						{{ $remision->descripcion_producto }}
					</td>
					<td style="border-right:1px solid #9a9a9a; padding-left: 10px;">
						{{ $remision->certificado_de_origen }}
					</td>
					<td style="border-right:1px solid #9a9a9a; padding-left: 10px;">
						{{ $remision->cantidad }}
					</td>
					<td>
						{{ $remision->total }}
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>

		<table style="width: 65%; float: left; border: 1px solid #9a9a9a; border-radius: 8px;  padding-top: 5px; margin-top: 1%; font-family: 'Nunito', sans-serif; font-size:14px;">
			<thead>
				<tr>
					<th style="text-align: left; border-bottom: 1px solid #9a9a9a; padding-left: 20px; font-family: 'Nunito', sans-serif; font-size:14px;">Son: </th>
				</tr>
				<tr>
					<th style="text-align: left; padding-left: 20px; font-family: 'Nunito', sans-serif; font-size:14px;">Comentarios: </th>
				</tr>
			</thead>
		</table>
		<table style="width: 33%; float: right; margin-left: 2%; border: 1px solid #9a9a9a; border-radius: 8px; padding-top: 5px; font-family: 'Nunito', sans-serif; font-size:14px;">
			<thead>
				<tr>
					<th style="text-align: left; padding-left: 20px; font-family: 'Nunito', sans-serif; font-size:14px;">Subtotal: </th>
				</tr>
				<tr>
					<th style="text-align: left; padding-left: 20px; font-family: 'Nunito', sans-serif; font-size:14px;">TOTAL: </th>
				</tr>
			</thead>
		</table>
		<p style="color: white; font-family: 'Nunito', sans-serif; font-size:14px;">salto de linea</p>
		<table id="lista_productos"  style="width: 100%; border: 1px solid #9a9a9a; border-radius: 8px; padding-top: 5px; padding-bottom: 5px; margin-top: -20px; font-family: 'Nunito', sans-serif; font-size:14px;">
			<thead>
				<tr>
					<th style="border-bottom: 1px solid #9a9a9a; padding-bottom: 5px; font-family: 'Nunito', sans-serif; font-size:14px;">Placa del vehiculo</th>
					<th style="border-bottom: 1px solid #9a9a9a; padding-bottom: 5px; font-family: 'Nunito', sans-serif; font-size:14px;">Identificacion del conductor</th>
					<th style="border-bottom: 1px solid #9a9a9a; padding-bottom: 5px; font-family: 'Nunito', sans-serif; font-size:14px;">Nombre del conductor</th>
					<th style="border-bottom: 1px solid #9a9a9a; padding-bottom: 5px; font-family: 'Nunito', sans-serif; font-size:14px;">Celular conductor</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					@foreach($remision_unica as $remision_uni)
						<td style="border-right:1px solid #9a9a9a; text-align: right; padding-right: 20px; font-family: 'Nunito', sans-serif; font-size:14px;">
							{{ $remision_uni->placa_conductor }}
						</td>
						<td style="border-right:1px solid #9a9a9a; padding-left: 10px; font-family: 'Nunito', sans-serif; font-size:14px;">
							{{ $remision_uni->identificacion_conductor }}
						</td>
						<td style="border-right:1px solid #9a9a9a; padding-left: 10px; font-family: 'Nunito', sans-serif; font-size:14px;">
							{{ $remision_uni->nombres_conductor }} {{ $remision_uni->apellidos_conductor }}
						</td>
						<td style="padding-left: 10px; font-family: 'Nunito', sans-serif; font-size:14px;">
							{{ $remision_uni->celular_1_conductor }} {{ $remision_uni->celular_2_conductor }}
						</td>
					@endforeach
				</tr>
			</tbody>
		</table>
	</div>
	<hr style="background-color: red; border: 1px solid green;">
	<div class="contenido" style="width: 100%;">
	<div style="width: 60%; float:left; border: 1px solid #9a9a9a; border-radius: 8px; padding-left: 20px; padding-top: 5px; padding-bottom: 5px; text-align: center; font-family: 'Nunito', sans-serif; font-size: 11px;">
			@foreach($parametros as $parametro)
			<p>
				<b>{{ $parametro->nombre_parametros }}</b> <br>
				Nit. {{ $parametro->identificacion_parametros }} R&eacute;gimen Co&uacute;m <br>
				{{ $parametro->direccion_parametros }} <br>
				Cel: {{ $parametro->telefono_fijo_parametros }} - {{ $parametro->celular_1_parametros }} - {{ $parametro->celular_2_parametros }} - {{ $parametro->celular_3_parametros }} <br>
				Email: {{ $parametro->email_cliente }} <br>
				{{ $parametro->id_ciudad_parametros }}
			</p>
			@endforeach
	</div>
	<div style="text-align:center; width: 30%; float: right; padding-left: 20px; padding-top: 5px; padding-bottom: 5px; font-family: 'Nunito', sans-serif; font-size:14px;">
			@foreach($parametros as $parametro)
				<img src="http://trazascol.softworldcolombia.com/img/icon-trazas.png" style="width: 100px;">
			@endforeach
		</div>
	</div>
	<div style="clear: both"></div>
	<div class="contenido" style="width: 100%;">
		@foreach($remision_unica as $remision_uni)
			<table class="tabla" style="width: 100%;">
				<thead>
					<tr>
						<th style="text-align: right; font-family: 'Nunito', sans-serif; font-size:14px;">REMISION No.</th>
						<td colspan="3" style="border: 1px solid #9a9a9a; font-family: 'Nunito', sans-serif; font-size:14px;">{{ $remision_uni->id_consecutivo }}</td>
					</tr>
				</thead>
			</table>
			<table style="width: 65%; float: left; border: 1px solid #9a9a9a; border-radius: 8px; padding-left: 20px; padding-top: 5px; padding-bottom: 5px; font-family: 'Nunito', sans-serif; font-size:14px;">
				<thead>
					<tr>
						<th style="text-align: left;">Nombre: </th>
						<td>{{ $remision_uni->nombres_cliente }}</td>
					</tr>
					<tr>
						<th style="text-align: left;">NIT: </th>
						<td>{{ $remision_uni->identificacion }}</td>
					</tr>
					<tr>
						<th style="text-align: left;">Direcci&oacute;n: </th>
						<td>{{ $remision_uni->direccion_cliente }}</td>
					</tr>
					<tr>
						<th style="text-align: left;">Ciudad: </th>
						<td>{{ $remision_uni->ciudad_cliente }}</td>
					</tr>
					<tr>
						<th style="text-align: left;">Telefono: </th>
						<td>{{ $remision_uni->telefono_fijo_cliente }} - {{ $remision_uni->celular_1_cliente }} - {{ $remision_uni->celular_2_cliente }}</td>
					</tr>
					<tr>
						<th style="text-align: left;">Email: </th>
						<td>{{ $remision_uni->email_cliente }}</td>
					</tr>
				</thead>
			</table>
			<table style="width: 33%; float: right; margin-left: 2%; border: 1px solid #9a9a9a; border-radius: 8px; padding-left: 20px; padding-top: 5px; padding-bottom: 5px; font-family: 'Nunito', sans-serif; font-size:14px;">
				<thead>
					<tr>
						<th style="text-align: left;">FECHA: </th>
						<td>{{ $remision_uni->fecha }}</td>
					</tr>
					<tr>
						<th style="text-align: left;">Vence: </th>
						<td >{{ $remision_uni->vence }}</td>
					</tr>
					<tr>
						<th style="text-align: left;">Orden: </th>
						<td>{{ $remision_uni->orden }}</td>
					</tr>
					<tr>
						<th>&nbsp;</th>
					</tr>
					<tr>
						<th>&nbsp;</th>
					</tr>
					<tr>
						<th>&nbsp;</th>
					</tr>
				</thead>
			</table>
		@endforeach
		<p style="color: white;">salto de linea</p>
		<table style="width: 100%;  border: 1px solid #9a9a9a; border-radius: 8px; padding-top: 5px; padding-bottom: 5px; margin-top: -30px; font-family: 'Nunito', sans-serif; font-size:14px;">
			<thead>
				<tr style="border: 1px solid #9a9a9a; padding-bottom: 5px;">
					<th style="border-bottom: 1px solid #9a9a9a; padding-bottom: 5px;">C&oacute;digo</th>
					<th style="border-bottom: 1px solid #9a9a9a; padding-bottom: 5px;">Descripci&oacute;n</th>
					<th style="border-bottom: 1px solid #9a9a9a; padding-bottom: 5px;">Certificado de origen</th>
					<th style="border-bottom: 1px solid #9a9a9a; padding-bottom: 5px;">Cantidad</th>
					<th style="border-bottom: 1px solid #9a9a9a; padding-bottom: 5px;">Total</th>
				</tr>
			</thead>
			<tbody>
				@foreach($remisiones as $remision)
				<tr>
					<td style="border-right:1px solid #9a9a9a; text-align: right; padding-right: 20px;">
						{{ $remision->id_producto }}
					</td>
					<td style="border-right:1px solid #9a9a9a; padding-left: 10px;">
						{{ $remision->descripcion_producto }}
					</td>
					<td style="border-right:1px solid #9a9a9a; padding-left: 10px;">
						{{ $remision->certificado_de_origen }}
					</td>
					<td style="border-right:1px solid #9a9a9a; padding-left: 10px;">
						{{ $remision->cantidad }}
					</td>
					<td>
						{{ $remision->total }}
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>

		<table style="width: 65%; float: left; border: 1px solid #9a9a9a; border-radius: 8px;  padding-top: 5px; margin-top: 1%; font-family: 'Nunito', sans-serif; font-size:14px;">
			<thead>
				<tr>
					<th style="text-align: left; border-bottom: 1px solid #9a9a9a; padding-left: 20px; font-family: 'Nunito', sans-serif; font-size:14px;">Son: </th>
				</tr>
				<tr>
					<th style="text-align: left; padding-left: 20px; font-family: 'Nunito', sans-serif; font-size:14px;">Comentarios: </th>
				</tr>
			</thead>
		</table>
		<table style="width: 33%; float: right; margin-left: 2%; border: 1px solid #9a9a9a; border-radius: 8px; padding-top: 5px; font-family: 'Nunito', sans-serif; font-size:14px;">
			<thead>
				<tr>
					<th style="text-align: left; padding-left: 20px; font-family: 'Nunito', sans-serif; font-size:14px;">Subtotal: </th>
				</tr>
				<tr>
					<th style="text-align: left; padding-left: 20px; font-family: 'Nunito', sans-serif; font-size:14px;">TOTAL: </th>
				</tr>
			</thead>
		</table>
		<p style="color: white; font-family: 'Nunito', sans-serif; font-size:14px;">salto de linea</p>
		<table id="lista_productos"  style="width: 100%; border: 1px solid #9a9a9a; border-radius: 8px; padding-top: 5px; padding-bottom: 5px; margin-top: -20px; font-family: 'Nunito', sans-serif; font-size:14px;">
			<thead>
				<tr>
					<th style="border-bottom: 1px solid #9a9a9a; padding-bottom: 5px; font-family: 'Nunito', sans-serif; font-size:14px;">Placa del vehiculo</th>
					<th style="border-bottom: 1px solid #9a9a9a; padding-bottom: 5px; font-family: 'Nunito', sans-serif; font-size:14px;">Identificacion del conductor</th>
					<th style="border-bottom: 1px solid #9a9a9a; padding-bottom: 5px; font-family: 'Nunito', sans-serif; font-size:14px;">Nombre del conductor</th>
					<th style="border-bottom: 1px solid #9a9a9a; padding-bottom: 5px; font-family: 'Nunito', sans-serif; font-size:14px;">Celular conductor</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					@foreach($remision_unica as $remision_uni)
						<td style="border-right:1px solid #9a9a9a; text-align: right; padding-right: 20px; font-family: 'Nunito', sans-serif; font-size:14px;">
							{{ $remision_uni->placa_conductor }}
						</td>
						<td style="border-right:1px solid #9a9a9a; padding-left: 10px; font-family: 'Nunito', sans-serif; font-size:14px;">
							{{ $remision_uni->identificacion_conductor }}
						</td>
						<td style="border-right:1px solid #9a9a9a; padding-left: 10px; font-family: 'Nunito', sans-serif; font-size:14px;">
							{{ $remision_uni->nombres_conductor }} {{ $remision_uni->apellidos_conductor }}
						</td>
						<td style="padding-left: 10px; font-family: 'Nunito', sans-serif; font-size:14px;">
							{{ $remision_uni->celular_1_conductor }} {{ $remision_uni->celular_2_conductor }}
						</td>
					@endforeach
				</tr>
			</tbody>
		</table>
	</div>
</body>
</html>