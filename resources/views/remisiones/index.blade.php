@extends('layouts.app')

@section('content')
<div class="content " >
    <div class="container-fluid" style="background-color: white; padding-top: 10px;">
        <div class="row">
            <div class="col-md-12">
              <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h3 class="card-title mt-0">Modulo de remisiones</h3>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                    @include('custom.message')
                  <div class="table-responsive">
                    <div class="form-group">
                        <label><b>Búsqueda de remisiones...</b></label>
                        <input type="text" class="form-control" id="busqueda_conductores" name="busqueda_conductores" placeholder="Digite nombre, apellido o placa del conductor a buscar">
                    </div>
                    <hr style="margin-top: 10px; margin-bottom: 20px;">
                    <table class="table table-hover table-bordered" id="resultado_consulta">
                        <thead>
                            <tr>
                                <th scope="col"><b>Remisi&oacute;n No.</b></th>
                                <th scope="col"><b>Orden de la Remisi&oacute;n</b></th>
                                <th scope="col"><b>Nombres y apellidos del cliente</b></th>
                                <th scope="col"><b>Nombres y apellidos del conductor</b></th>
                                <th scope="col"><b>Fecha registro</b></th>
                                <th scope="col" style="text-align: center;" colspan="2"><b>Acciones</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                             @foreach($remisiones as $remision)
                             	<tr>
                             		<td>{{ $remision->id_consecutivo }}</td>
                             		<td>
                             			{{ $remision->orden }}
                             		</td>
                             		<td>
                             			{{ $remision->nombres_cliente }} {{ $remision->apellidos_cliente }}
                             		</td>
                             		<td>
                             			{{ $remision->nombres_conductor }} {{ $remision->apellidos_conductor }}
                             		</td>
                             		<td>
                             			{{ $remision->fecha }}
                             		</td>
                             		<td style="text-align: center;">
	                                    <button type="button" class="btn btn-info btnVer" title="Ver registro" data-id="{{ $remision->id_consecutivo }}" data-url="{{ route('remisiones.show', $remision->id_consecutivo ) }}"><i class="fas fa-eye"></i></button>
                                        <a href="{{ route('remisiones.edit', $remision->id_consecutivo ) }}">
	                                    <button type="button" class="btn btn-success" title="Editar registro" data-id="{{ $remision->id_consecutivo }}" data-url=""><i class="far fa-edit"></i></button></a>
                                        
                                	</td>
                                    <td>
                                        <a href="{{ route('remisiones.pdf', $remision->id_consecutivo ) }}">
                                            <button type="button" class="btn btn-danger" title="Exportar a PDF"><i class="fas fa-file-pdf"></i></button>
                                        </a>
                                        <!--
                                        <button type="button" class="btn btn-success btnEdicion" title="Exportar a EXCEL" data-id="{{ $remision->id_consecutivo }}" data-url="{{ route('remisiones.edit', $remision->id_consecutivo ) }}"><i class="fas fa-file-excel"></i></button>-->
                                        <button type="button" class="btn btn-info imprimir" title="Imprimir" data-id="{{ $remision->id_consecutivo }}" data-url="{{ route('remisiones.imprimir', $remision->id_consecutivo ) }}"><i class="fas fa-print"></i></button>
                                    </td>
                                <td>
                                    <center>
                                    <form method="POST" action="{{ route('remisiones.destroy', $remision->id_consecutivo)}}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-warning" title="Eliminar registro">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </form>
                                    </center>
                                </td>
                             	</tr>
                             @endforeach
                        </tbody>
                    </table>

                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>


@endsection
