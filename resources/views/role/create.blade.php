@extends('layouts.app')

@section('content')

<div class="content">
  <div class="container-fluid" style="background-color: white; padding-top: 10px;">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-plain">
          <div class="card-header card-header-primary">
            <h3 class="card-title mt-0">Registrar un Rol en el Sistema</h3>
          </div>

          <div class="card-body">
            @include('custom.message')
            <div class="table-responsive">
              <form action="{{ route('role.store')}}" method="POST">
                @csrf

                <h3><b>Información requerida</b></h3>

                <div class="form-group">
                  <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="{{ old('name')}}" />
                </div>

                <div class="form-group">
                  <input type="text" class="form-control" id="slug" placeholder="Slug" name="slug" value="{{ old('slug')}}" />
                </div>

                <div class="form-group">
                  <textarea class="form-control" placeholder="Description" name="description" id="description" rows="3"> 
                    {{ old('description')}}
                  </textarea>
                </div>

                <h3><b>¿Conceder acceso administrativo?</b></h3>

                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="fullaccessyes" name="full-access" class="custom-control-input" value="yes"
                            @if (old('full-access')=="yes") 
                              checked 
                            @endif
                  />
                  <label class="custom-control-label" for="fullaccessyes">Si</label>
                </div>

                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="fullaccessno" name="full-access" class="custom-control-input" value="no" 
                            @if (old('full-access')=="no") 
                              checked 
                            @endif
                            @if (old('full-access')===null) 
                              checked 
                            @endif
                  />
                  <label class="custom-control-label" for="fullaccessno">No</label>
                </div>

                <h3><b>Listado de Permisos</b></h3>

                @foreach($permissions as $permission)

                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="permission_{{$permission->id}}" value="{{$permission->id}}" name="permission[]"
                    @if( is_array(old('permission')) && in_array("$permission->id", old('permission'))    )
                      checked
                    @endif
                  />

                  <label class="custom-control-label" for="permission_{{$permission->id}}">
                    {{ $permission->id }}
                    - 
                    {{ $permission->name }} 
                    <em>( {{ $permission->description }} )</em>
                  </label>
                </div>

                @endforeach

                <center>
                  <a href="{{route('role.index')}}">
                    <button class="btn btn-danger" type="button">
                      <i class="material-icons">keyboard_backspace</i> Regresar
                    </button>
                  </a>

                  <button class="btn btn-primary" type="submit">
                    <i class="material-icons">how_to_reg</i> Registrar
                  </button>
                </center>
              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
