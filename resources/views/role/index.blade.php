@extends('layouts.app')

@section('content')
<div class="content " >
    <div class="container-fluid" style="background-color: white; padding-top: 10px;">
        <div class="row">
            <div class="col-md-12">
              <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h3 class="card-title mt-0">Modulo de Roles</h3>
                  <p class="card-category">Acá puede ver todos los roles registrados en el sistema</p>
                </div>

                <div class="card-body">
                    @include('custom.message')
                  <div class="table-responsive">
                    @can('haveaccess','role.create')
                    <a href="{{route('role.create')}}" 
                       class="btn btn-primary float-right"
                       >Crear rol
                    </a>
                    <br><br>
                    @endcan
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col"><b>Nombre</b></th>
                                <th scope="col"><b>Slug</b></th>
                                <th scope="col"><b>Descripción</b></th>
                                <th scope="col"><b>¿Acceso total?</b></th>
                                <th colspan="3" style="text-align: center;"><b>Acciones</b></th>
                            </tr>
                        </thead>
                        <tbody>


                            @foreach ($roles as $role)
                            <tr>
                                <th scope="row">{{ $role->id}}</th>
                                <td>{{ $role->name}}</td>
                                <td>{{ $role->slug}}</td>
                                <td>{{ $role->description}}</td>
                                <td>{{ $role['full-access']}}</td>                            
                                <td> 
                                    @can('haveaccess','role.show')
                                    <a class="btn btn-info" href="{{ route('role.show',$role->id)}}">
                                        <i class="material-icons">visibility</i>
                                    </a> 
                                    @endcan 
                                </td>  
                                <td> 
                                    @can('haveaccess','role.edit')
                                    <a class="btn btn-success" href="{{ route('role.edit',$role->id)}}">
                                        <i class="material-icons">create</i>
                                    </a> 
                                    @endcan
                                </td>  

                                <td> 
                                    @can('haveaccess','role.destroy')
                                    <form action="{{ route('role.destroy',$role->id)}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger">
                                            <i class="material-icons">delete_forever</i>
                                        </button>
                                    </form>
                                    @endcan

                                </td>  
                            </tr>      
                            @endforeach

                        </tbody>
                    </table>

                    {{ $roles->links() }}

                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>

@endsection
