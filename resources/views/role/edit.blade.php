@extends('layouts.app')

@section('content')
<div class="content " >
    <div class="container-fluid" style="background-color: white; padding-top: 10px;">
        <div class="row">
            <div class="col-md-12">
              <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h3 class="card-title mt-0">Edición rol seleccionado</h3>
                  <!-- <p class="card-category">Acá puede ver todos los roles registrados en el sistema</p> -->
                </div>

                <div class="card-body">
                    @include('custom.message')
                    <div class="table-responsive">
                      <form action="{{ route('role.update', $role->id)}}" method="POST">
                        @csrf
                        @method('PUT')

                         <div class="form-group">                            
                            <input type="text" class="form-control" 
                            id="name" 
                            placeholder="Nombre"
                            name="name"
                            value="{{ old('name', $role->name)}}"
                            >
                          </div>
                          <div class="form-group">                            
                            <input type="text" 
                            class="form-control" 
                            id="slug" 
                            placeholder="Slug"
                            name="slug"
                            value="{{ old('slug' , $role->slug)}}"
                            >
                          </div>

                          <div class="form-group">
                            
                            <textarea class="form-control" placeholder="Descripción" name="description" id="description" rows="3">{{old('description', $role->description)}}</textarea>
                          </div>

                          <hr>

                          <h3>Acceso de administrador</h3>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="fullaccessyes" name="full-access" class="custom-control-input" value="yes"
                            @if ( $role['full-access']=="yes") 
                              checked 
                            @elseif (old('full-access')=="yes") 
                              checked 
                            @endif
                            
                            
                            >
                            <label class="custom-control-label" for="fullaccessyes">Acceso total</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="fullaccessno" name="full-access" class="custom-control-input" value="no" 
                            
                            @if ( $role['full-access']=="no") 
                              checked 
                            @elseif (old('full-access')=="no") 
                              checked 
                            @endif
                            
                            
                            >
                            <label class="custom-control-label" for="fullaccessno">Acceso Personalizado</label>
                          </div>

                          <hr>


                          <h3>Permisos del sistema</h3>


                          @foreach($permissions as $permission)

                          
                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" 
                            class="custom-control-input" 
                            id="permission_{{$permission->id}}"
                            value="{{$permission->id}}"
                            name="permission[]"

                            @if( is_array(old('permission')) && in_array("$permission->id", old('permission'))    )
                            checked

                            @elseif( is_array($permission_role) && in_array("$permission->id", $permission_role)    )
                            checked

                            @endif
                            >
                            <label class="custom-control-label" 
                                for="permission_{{$permission->id}}">
                                {{ $permission->id }}
                                - 
                                {{ $permission->name }} 
                                <em>( {{ $permission->description }} )</em>
                            
                            </label>
                          </div>


                          @endforeach
                          <hr>
                          <center>

                            <a href="{{route('role.index')}}">
                              <button class="btn btn-danger" type="button">
                                <i class="material-icons">keyboard_backspace</i> Regresar
                              </button>
                            </a>

                            <button class="btn btn-primary" type="submit">
                              <i class="material-icons">update</i> Actualizar
                            </button>
                            
                          </center>

                     </div>
                   </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>

@endsection
