@foreach($clientes as $cliente)
<div class="container">
	 @include('custom.parameters')
	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>(*) Tipo de identificación:</b></label>
				<select class="form-control" name="tipo_identificacion" readonly="" disabled="">
					@foreach($tipo_identificacion as $tipo)
						@if($cliente->tipo_id_cliente == $tipo->id_tipo)
							<option value="{{ $tipo->id_tipo }}" selected="">{{ $tipo->descripcion_tipo }}</option>
						@else
							<option value="{{ $tipo->id_tipo }}">{{ $tipo->descripcion_tipo }}</option>
						@endif
					@endforeach
				</select>
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>(*) Número de identificación:</b></label>
				<input type="text" class="form-control" placeholder="" name="identificacion" value="{{ $cliente->identificacion }}" disabled="" />
			</div>
		</div>
	</div>

	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<?php 
				$array_nombre = explode(" ", $cliente->nombres_cliente);
			?>
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>(*) Primer nombre:</b></label>
				<input type="text" class="form-control" placeholder="" name="primer_nombre" value="{{ $array_nombre[0] }}" disabled="">
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>Segundo nombre:</b></label>
				<input type="text" class="form-control" placeholder="" name="segundo_nombre" value="{{ $array_nombre[1] }}" disabled="">
			</div>
		</div>
	</div>

	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<?php 
				$array_apellido = explode(" ", $cliente->apellidos_cliente);
			?>
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>(*) Primer apellido</b></label>
				<input type="text" class="form-control" placeholder="" name="primer_apellido" value="{{ $array_apellido[0] }}" disabled="">
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b> Segundo apellido:</b></label>
				<input type="text" class="form-control" placeholder="" name="segundo_apellido" value="{{ $array_apellido[1] }}" disabled="">
			</div>
		</div>
	</div>

	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<label style="padding-bottom: 5px;"><b>(*) Direcci&oacute;n:</b></label>
		<input type="text" class="form-control" placeholder="" name="direccion" value="{{ $cliente->direccion_cliente }}" disabled="">
	</div>

	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<label style="padding-bottom: 5px;"><b>(*) Ciudad</b></label>
		<input type="text" class="form-control" placeholder="" name="ciudad" value="{{ $cliente->ciudad_cliente }}" disabled="">
	</div>

	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>Telefono fijo</b></label>
				<input type="text" class="form-control" placeholder="" name="telefono_fijo" value="{{ $cliente->telefono_fijo_cliente }}" disabled="">
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b>(*) Celular:</b></label>
				<input type="text" class="form-control" placeholder="" name="celular" value="{{ $cliente->celular_1_cliente }}" disabled="">
			</div>
		</div>
	</div>

	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row">
			<div class="col-12 col-sm-6 col-md-6 col-lg-6"></div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<label style="padding-bottom: 5px;"><b> Celular 2:</b></label>
				<input type="text" class="form-control" placeholder="" name="celular_2" value="{{ $cliente->celular_2_cliente }}" disabled="">
			</div>
		</div>
	</div>

	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<label style="padding-bottom: 5px;"><b>(*) Correo electronico</b></label>
		<input type="email" class="form-control" placeholder="" name="correo" value="{{ $cliente->email_cliente }}" disabled="">
	</div>
	
	<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
		<label style="padding-bottom: 5px;"><b>(*) Estado cliente</b></label>
		<select class="form-control" name="estado" disabled="">
			@if($cliente->estado_cliente == '1')
				<option value="1" selected="">Activo</option>
				<option value="0">Inactivo</option>
			@else
				<option value="1">Activo</option>
				<option value="0" selected="">Inactivo</option>
			@endif
		</select>
	</div>

</div>
@endforeach