<thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col"><b>Identificacion</b></th>
                                <th scope="col"><b>Nombre</b></th>
                                <th scope="col"><b>Apellido</b></th>
                                <th scope="col"><b>Correo</b></th>
                                <th scope="col" style="text-align: center;" colspan="2"><b>Acciones</b></th>
                            </tr>
                        </thead>
                        <tbody>
                             @foreach($clientes as $cliente)
                             <?php $i=1; ?>
                             <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $cliente->identificacion }}</td>
                                <td>{{ $cliente->nombres_cliente }}</td>
                                <td>{{ $cliente->apellidos_cliente }}</td>
                                <td>{{ $cliente->email_cliente }}</td>
                                <td style="text-align: center;">
                                    <button type="button" class="btn btn-info btnVer" title="Ver registro" data-id="{{ $cliente->id }}" data-url="{{ route('clientes.show', $cliente->id ) }}"><i class="fas fa-eye"></i></button>
                                    <button type="button" class="btn btn-success btnEdicion" title="Editar registro" data-id="{{ $cliente->id }}" data-url="{{ route('clientes.edit', $cliente->id ) }}"><i class="far fa-edit"></i></button>
                                </td>
                                <td>
                                    <center>
                                    <form method="POST" action="{{ route('clientes.destroy', $cliente->id)}}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger" title="Eliminar registro">
                                            <i class="fas fa-user-times"></i>
                                        </button>
                                    </form>
                                    </center>
                                </td>
                             </tr>
                             @endforeach
                        </tbody>