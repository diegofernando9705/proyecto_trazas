@extends('layouts.app')

@section('content')
<div class="content " >
    <div class="container-fluid" style="background-color: white; padding-top: 10px;">
        <div class="row">
            <div class="col-md-12">
              <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h3 class="card-title mt-0">Modulo de clientes</h3>
                  <p class="card-category">Acá puede ver todos los clientes registrados en el sistema</p>
                </div>
                <div class="card-body">
                    @include('custom.message')
                  <div class="table-responsive">
                    <div class="form-group">
                        <label><b>Búsqueda de clientes...</b></label>
                        <input type="text" class="form-control" id="busqueda_cliente" name="busqueda_cliente" placeholder="Digite nombre, apellido o identificacion del cliente a buscar">
                    </div>
                    <hr style="margin-top: 10px; margin-bottom: 20px;">
                    <table class="table table-hover table-bordered" id="resultado_consulta">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col"><b>Identificacion</b></th>
                                <th scope="col"><b>Nombre</b></th>
                                <th scope="col"><b>Apellido</b></th>
                                <th scope="col"><b>Correo</b></th>
                                <th scope="col"><b>Estado</b></th>
                                <th scope="col" style="text-align: center;" colspan="2"><b>Acciones</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                             @foreach($clientes as $cliente)
                             
                             <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $cliente->identificacion }}</td>
                                <td>{{ $cliente->nombres_cliente }}</td>
                                <td>{{ $cliente->apellidos_cliente }}</td>
                                <td>{{ $cliente->email_cliente }}</td>
                                @if($cliente->estado_cliente == '1')
                                    <td class="alert alert-success">Activo</td>
                                @else
                                    <td class="alert alert-danger">Inactivo</td>
                                @endif
                                <td style="text-align: center;">
                                    <button type="button" class="btn btn-info btnVer" title="Ver registro" data-id="{{ $cliente->id }}" data-url="{{ route('clientesRemisiones.show', $cliente->id ) }}"><i class="fas fa-eye"></i></button>
                                    <button type="button" class="btn btn-success btnEdicion" title="Editar registro" data-id="{{ $cliente->id }}" data-url="{{ route('clientesRemisiones.edit', $cliente->id ) }}"><i class="far fa-edit"></i></button>
                                </td>
                                <td>
                                    <center>
                                    <form method="POST" action="{{ route('clientesRemisiones.destroy', $cliente->id)}}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger" title="Eliminar registro">
                                            <i class="fas fa-user-times"></i>
                                        </button>
                                    </form>
                                    </center>
                                </td>
                             </tr>
                             @endforeach
                        </tbody>
                    </table>
                    {{ $clientes->links() }}
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>


@endsection
