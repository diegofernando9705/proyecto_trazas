$(document).ready(function () {
    console.log("%cProgramado y Desarrollado por Sotworld Colombia https://softworldcolombia.com/", "color: blue; font-family:'Comic Sans MS'");
    console.warn("Programado y Desarrollado por Sotworld Colombia https://softworldcolombia.com/");
    console.image("https://softworldcolombia.com/wp-content/uploads/2020/09/title-softworld.png");

    $(document).ready(function () {
        $('.mi-selector').select2();
        $('.mi-selector-tabla').select2();
        $('.mi-selector-tabla-producto').select2();

    });

	$(document).on("click",".btnVer",function(){
		
		$('#ver_modales').modal({
            show: true
        });

		href = $(this).attr("data-url");
        
        $.ajax({
            url: href,
            method: 'get',
            success: function(data){
                $("#modal-body").html(data);
            }
        });
	});

    $(document).on("click",".btnEdicion",function(){
        
        $('#ver_modales').modal({
            show: true
        });

        href = $(this).attr("data-url");
        
        $.ajax({
            url: href,
            method: 'get',
            success: function(data){
                $("#modal-body").html(data);
            }
        });
    });

    $(document).on("keyup", "#busqueda_cliente", function(){
        var data = $(this).val();
        $.ajax({
            url: '/remisiones/keyup/clientes/' + data,
            method: 'get',
            success: function(data){
                $("#resultado_consulta").html(data);
            }
        });
    });

    $(document).on("keyup", "#busqueda_producto", function(){
        var data = $(this).val();
        $.ajax({
            url: '/remisiones/keyup/productos/' + data,
            method: 'get',
            success: function(data){
                $("#resultado_consulta").html(data);
            }
        });
    });

    $(document).on("keyup", "#busqueda_conductores", function(){
        var data = $(this).val();
        $.ajax({
            url: '/remisiones/keyup/conductores/' + data,
            method: 'get',
            success: function(data){
                $("#resultado_consulta").html(data);
            }
        });
    });

    /* funcion busqueda cliente */
    $(document).on("change",".mi-selector",function(){
        

        if($(this).attr('data-informacion')){
            
            var identificacion = $(this).val();
            var atributo = $(this).attr('data-informacion');
            $.ajax({
                url: '/remisiones/onchange/conductor/' + atributo + '/' + identificacion,
                method: 'get',
                success: function(data){
                    $("#placa_conductor").val(data[0]['placa_conductor']).trigger("change.select2");
                    $("#identificacion_conductor").val(data[0]['identificacion_conductor']).trigger("change.select2");
                    $("#nombre_conductor").val(data[0]['identificacion_conductor']).trigger("change.select2");
                    $("#celular_conductor").val(data[0]['identificacion_conductor']).trigger("change.select2");
                }
            });

        }else{

            var identificacion = $(this).val();
        
            $.ajax({
                url: '/remisiones/onchange/cliente/' + identificacion,
                method: 'get',
                success: function(data){
                    $("#identificacion_formulario").val(data[0]['identificacion']);
                    $("#direccion_formulario").val(data[0]['direccion_cliente']);
                    $("#ciudad_formulario").val(data[0]['ciudad_cliente']);
                    $("#telefono_formulario").val(data[0]['telefono_fijo_cliente']+" - "+data[0]['telefono_fijo_cliente']+" - "+data[0]['celular_1_cliente']+" - "+data[0]['celular_2_cliente']);
                    $("#email_formulario").val(data[0]['email_cliente']);

                    console.log(data[0]['identificacion']);
                }
            });

        }
    });

    /* funcion busqueda cliente */
    $(document).on("change",".mi-selector-tabla",function(){
        var valor = $(this).val();
        var ide = $(this).attr('data-id');
        var id = "#"+ide;
        
        $.ajax({
            url: '/remisiones/onchange/producto_tabla/' + valor,
            method: 'get',
            success: function(data){
                $(id).val(valor).trigger("change.select2");
                console.log(data[0]['descripcion_producto']);
            }
        });
    });

    $(document).on("change",".mi-selector-tabla-producto",function(){
            
        var valor = $(this).val();
        var ide = $(this).attr('data-id');
        var id_co = "#codigo_"+ide;

        $.ajax({
            url: '/remisiones/onchange/producto_tabla/' + valor,
            method: 'get',
            success: function(data){
                $(id_co).val(valor).trigger("change.select2");
                $(id_co).removeClass('mi-selector-tabla-producto');

                console.log(data[0]['descripcion_producto']);
            }
        });
    });


    var tbody = $('#lista_productos tbody');
    var fila_contenido = tbody.find('tr').first().html();
    //Agregar fila nueva.

    $(document).on("click", ".button_agregar_producto", function(){
        var fila_nueva = $('<tr><td></td></tr>');
        fila_nueva.append(fila_contenido);
        tbody.append(fila_nueva);
    });

 
    $(document).on("click", ".button_eliminar_producto", function(){    
        $(this).parents('tr').eq(0).remove();
    });
    
    
    $(document).on("click", ".imprimir", function(){

        href = $(this).attr("data-url");

        $.ajax({
            url: href,
            method: 'get',
            success: function(data){
                var printContents = "VOLVER";
                var document_html = window.open("_blank", 'Impresion', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=1,width=800,height=700,left = 390,top = 50');
                 document_html.document.write( "<html><head><title></title>" );
                 document_html.document.write( "<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css' integrity='sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2' crossorigin='anonymous'>" );
                 document_html.document.write( "<link rel=\"stylesheet\" href=\"https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css\" type=\"text/css\"/>" );
                 document_html.document.write( "</head><body>" );
                 document_html.document.write(data);
                 document_html.document.write( "</body></html>" );
                 setTimeout(function () {
                       document_html.print();
                   }, 500)
            }
        });
            
    });

});