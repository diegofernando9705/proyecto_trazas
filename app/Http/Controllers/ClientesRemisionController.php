<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ClientesRemisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = DB::table('tabla_clientes_remisiones')->paginate(10);
        return view('clientes.index', compact('clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $tipo_identificacion = DB::table('tabla_tipo_identificacion')->get();
        return view('clientes.crear', compact('tipo_identificacion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'tipo_identificacion' => 'required',
            'identificacion' => 'required',
            'primer_nombre' => 'required',
            'primer_apellido' => 'required',
            'direccion' => 'required',
            'ciudad' => 'required',
            'celular' => 'required',
            'correo' => 'required',
            'estado' => 'required'
        ]);

        $clientes = DB::table('tabla_clientes_remisiones')->insert([
            'tipo_id_cliente' => $request->tipo_identificacion,
            'identificacion' => $request->identificacion,
            'nombres_cliente' => $request->primer_nombre." ".$request->segundo_nombre,
            'apellidos_cliente' => $request->primer_apellido." ".$request->segundo_apellido,
            'direccion_cliente' => $request->direccion,
            'ciudad_cliente' => $request->ciudad,
            'telefono_fijo_cliente' => $request->telefono_fijo,
            'celular_1_cliente' => $request->celular,
            'celular_2_cliente' => $request->celular_2,
            'email_cliente' => $request->correo,
            'estado_cliente' => $request->estado
        ]);

        
        return redirect()->route('clientesRemisiones.index')->with('status_success','Registro exitoso!'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $clientes = DB::table('tabla_clientes_remisiones')
                    ->where('id', $id)
                    ->get();

        $tipo_identificacion = DB::table('tabla_tipo_identificacion')->get();

        return view('clientes.ver', compact('clientes', 'tipo_identificacion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $clientes = DB::table('tabla_clientes_remisiones')
                    ->where('id', $id)
                    ->get();

        $tipo_identificacion = DB::table('tabla_tipo_identificacion')->get();

        return view('clientes.edicion', compact('clientes', 'tipo_identificacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'tipo_identificacion' => 'required',
            'identificacion' => 'required',
            'primer_nombre' => 'required',
            'primer_apellido' => 'required',
            'direccion' => 'required',
            'ciudad' => 'required',
            'celular' => 'required',
            'correo' => 'required',
            'estado' => 'required'
        ]);

        $clientes = DB::table('tabla_clientes_remisiones')->where('id', $id)->update([
            'tipo_id_cliente' => $request->tipo_identificacion,
            'identificacion' => $request->identificacion,
            'nombres_cliente' => $request->primer_nombre." ".$request->segundo_nombre,
            'apellidos_cliente' => $request->primer_apellido." ".$request->segundo_apellido,
            'direccion_cliente' => $request->direccion,
            'ciudad_cliente' => $request->ciudad,
            'telefono_fijo_cliente' => $request->telefono_fijo,
            'celular_1_cliente' => $request->celular,
            'celular_2_cliente' => $request->celular_2,
            'email_cliente' => $request->correo,
            'estado_cliente' => $request->estado
        ]);

        
        return redirect()->route('clientesRemisiones.index')->with('status_success','Actualizacion exitosa!');//
    }

    public function keyup_cliente($data){
        
        $clientes = DB::table('tabla_clientes_remisiones')
                    ->where("identificacion", 'like', $data . "%")
                    ->orWhere("nombres_cliente", 'like', $data . "%")
                    ->orWhere("apellidos_cliente", 'like', $data . "%")->get();

        return view('clientes.keyup', compact('clientes'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('tabla_clientes_remisiones')->where('id', $id)->update([
            'estado_cliente' => '0',
        ]);

        return redirect()->route('clientesRemisiones.index')
            ->with('status_success','Borrado exitoso!'); 
    }
}
