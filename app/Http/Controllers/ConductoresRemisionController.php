<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ConductoresRemisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conductores = DB::table('tabla_conductor_remision')->paginate(10);

        return view('conductores.index', compact('conductores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipo_identificacion = DB::table('tabla_tipo_identificacion')->get();
        return view('conductores.crear', compact('tipo_identificacion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tipo_identificacion' => 'required',
            'identificacion' => 'required',
            'primer_nombre' => 'required',
            'primer_apellido' => 'required',
            'celular' => 'required',
            'placa_vehiculo' => 'required',
            'estado' => 'required'
        ]);

        
        DB::table('tabla_conductor_remision')->insert([
            'id_tipo_conductor' => $request->tipo_identificacion,
            'identificacion_conductor' => $request->identificacion,
            'nombres_conductor' => $request->primer_nombre." ".$request->segundo_nombre,
            'apellidos_conductor' => $request->primer_apellido." ".$request->segundo_apellido,
            'celular_1_conductor' => $request->celular,
            'celular_2_conductor' => $request->celular_opcional,
            'placa_conductor' => $request->placa_vehiculo,
            'estado_conductor' => $request->estado
        ]);

        return redirect()->route('conductoresRemisiones.index')->with('status_success','Registro exitoso!'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $conductores = DB::table('tabla_conductor_remision')
                    ->where('id', $id)
                    ->get();

        $tipo_identificacion = DB::table('tabla_tipo_identificacion')->get();

        return view('conductores.ver', compact('conductores', 'tipo_identificacion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $conductores = DB::table('tabla_conductor_remision')
                    ->where('id', $id)
                    ->get();

        $tipo_identificacion = DB::table('tabla_tipo_identificacion')->get();

        return view('conductores.edicion', compact('conductores', 'tipo_identificacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tipo_identificacion' => 'required',
            'identificacion' => 'required',
            'primer_nombre' => 'required',
            'primer_apellido' => 'required',
            'celular' => 'required',
            'placa_vehiculo' => 'required',
            'estado' => 'required'
        ]);

        DB::table('tabla_conductor_remision')->where('id', $id)->update([
            'id_tipo_conductor' => $request->tipo_identificacion,
            'identificacion_conductor' => $request->identificacion,
            'nombres_conductor' => $request->primer_nombre." ".$request->segundo_nombre,
            'apellidos_conductor' => $request->primer_apellido." ".$request->segundo_apellido,
            'celular_1_conductor' => $request->celular,
            'celular_2_conductor' => $request->celular_opcional,
            'placa_conductor' => $request->placa_vehiculo,
            'estado_conductor' => $request->estado
        ]);

        return redirect()->route('conductoresRemisiones.index')->with('status_success','Actualizacion exitosa!'); 
    }

    public function keyup_conductores($data){
        
        $conductores = DB::table('tabla_conductor_remision')
                    ->where("identificacion_conductor", 'like', $data . "%")
                    ->orWhere("nombres_conductor", 'like', $data . "%")
                    ->orWhere("apellidos_conductor", 'like', $data . "%")
                    ->orWhere("placa_conductor", 'like', $data . "%")->paginate(10);

        return view('conductores.keyup', compact('conductores'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        DB::table('tabla_conductor_remision')->where('id', $id)->update([
            'estado_conductor' => '0',
        ]);

        return redirect()->route('conductoresRemisiones.index')
            ->with('status_success','Borrado exitoso!');
    }
}
