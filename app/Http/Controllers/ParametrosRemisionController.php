<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ParametrosRemisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parametros = DB::table('tabla_parametros_remision')->paginate(10);
        return view('parametros.index', compact('parametros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $tipo_identificacion = DB::table('tabla_tipo_identificacion')->get();
        return view('parametros.crear', compact('tipo_identificacion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'tipo_identificacion' => 'required',
            'identificacion' => 'required',
            'logo' => 'required|mimes:jpg,jpeg,bmp,png',
            'razon_social' => 'required',
            'direccion' => 'required',
            'ciudad' => 'required',
            'celular' => 'required',
            'email' => 'required',
            'estado' => 'required',
        ]);

        //obtenemos el campo file definido en el formulario
        $file = $request->file('logo');
        //obtenemos el nombre del archivo
        $logo = $file->getClientOriginalName();
        //obtenemos el nombre del archivo
        $logo = $file->getClientOriginalName('logo');
        \Storage::disk('public')->put($logo, \File::get($file));

        DB::table('tabla_parametros_remision')->insert([
            'id_tipo_parametro' => $request->tipo_identificacion,
            'identificacion_parametros' => $request->identificacion,
            'logo_parametros' => $logo,
            'nombre_parametros' => $request->razon_social,
            'direccion_parametros' => $request->direccion,
            'id_ciudad_parametros' => $request->ciudad,
            'telefono_fijo_parametros' => $request->telefono_fijo,
            'celular_1_parametros' => $request->celular,
            'celular_2_parametros' => $request->celular_1,
            'celular_3_parametros' => $request->celular_2,
            'email_cliente' => $request->email,
            'estado_parametro' => $request->estado
        ]);

        return redirect()->route('parametrosRemisiones.index')->with('status_success','Registro exitoso!'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $parametros = DB::table('tabla_parametros_remision')
                    ->where('id', $id)
                    ->get();

        $tipo_identificacion = DB::table('tabla_tipo_identificacion')->get();

        return view('parametros.ver', compact('parametros', 'tipo_identificacion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parametros = DB::table('tabla_parametros_remision')
                    ->where('id', $id)
                    ->get();

        $tipo_identificacion = DB::table('tabla_tipo_identificacion')->get();

        return view('parametros.edicion', compact('parametros', 'tipo_identificacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tipo_identificacion' => 'required',
            'identificacion' => 'required',
            'logo' => 'mimes:jpg,jpeg,bmp,png',
            'razon_social' => 'required',
            'direccion' => 'required',
            'ciudad' => 'required',
            'celular' => 'required',
            'email' => 'required',
            'estado' => 'required',
        ]);
        
        if(empty($request->logo)){
            $logo_parametro = $request->imagen_bd;
        }else{
            //obtenemos el nombre del archivo
             //obtenemos el campo file definido en el formulario
            $file = $request->file('logo');
            $logo_parametro = $file->getClientOriginalName('logo');
            \Storage::disk('public')->put($logo_parametro, \File::get($file));
        }

         DB::table('tabla_parametros_remision')->where('id', $id)->update([
            'id_tipo_parametro' => $request->tipo_identificacion,
            'identificacion_parametros' => $request->identificacion,
            'logo_parametros' => $logo_parametro,
            'nombre_parametros' => $request->razon_social,
            'direccion_parametros' => $request->direccion,
            'id_ciudad_parametros' => $request->ciudad,
            'telefono_fijo_parametros' => $request->telefono_fijo,
            'celular_1_parametros' => $request->celular,
            'celular_2_parametros' => $request->celular_1,
            'celular_3_parametros' => $request->celular_2,
            'email_cliente' => $request->email,
            'estado_parametro' => $request->estado
        ]);

        return redirect()->route('parametrosRemisiones.index')->with('status_success','Registro exitoso!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('tabla_parametros_remision')->where('id', $id)->update([
            'estado_parametro' => '0',
        ]);

        return redirect()->route('parametrosRemisiones.index')
            ->with('status_success','Borrado exitoso!'); 
    }
}
