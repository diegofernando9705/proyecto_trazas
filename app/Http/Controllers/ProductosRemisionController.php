<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ProductosRemisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = DB::table('tabla_productos_remisiones')->paginate(10);

        return view('productos.index', compact('productos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('productos.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre_producto' => 'required',
            'estado' => 'required'
        ]);

         DB::table('tabla_productos_remisiones')->insert([
            'descripcion_producto' => $request->nombre_producto,
            'estado_producto' => $request->estado
         ]);

         return redirect()->route('productosRemisiones.index')->with('status_success','Registro exitoso!'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $productos = DB::table('tabla_productos_remisiones')->where('id_producto', $id)->get();

        return view('productos.edicion', compact('productos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre_producto' => 'required',
            'estado' => 'required'
        ]);

        $productos = DB::table('tabla_productos_remisiones')->where('id_producto', $id)->update([
            'descripcion_producto' => $request->nombre_producto,
            'estado_producto' => $request->estado
        ]);
        
        return redirect()->route('productosRemisiones.index')->with('status_success','Actualizacion exitosa!'); 

    }

    public function keyup_producto($data){
        
        $productos = DB::table('tabla_productos_remisiones')
                    ->where("descripcion_producto", 'like', $data . "%")
                    ->orWhere("id_producto", 'like', $data . "%")
                    ->get();

        return view('productos.keyup', compact('productos'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('tabla_productos_remisiones')->where('id_producto', $id)->update([
            'estado_producto' => '0',
        ]);

        return redirect()->route('productosRemisiones.index')
            ->with('status_success','Borrado exitoso!');
    }
}
