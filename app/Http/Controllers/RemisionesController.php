<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Barryvdh\DomPDF\Facade as PDF;

class RemisionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $remisiones = DB::table('tabla_remisiones')
                    ->select('tabla_clientes_remisiones.*', 'tabla_conductor_remision.*', 'tabla_remisiones.*')
                    ->join('tabla_clientes_remisiones', 'tabla_remisiones.id_cliente', '=', 'tabla_clientes_remisiones.identificacion')
                    ->join('tabla_conductor_remision', 'tabla_remisiones.id_conductor_remision', '=', 'tabla_conductor_remision.identificacion_conductor')
                    ->where('estado', 1)
                    ->paginate(10);
        /*
        $remisiones = DB::table('tabla_detalle_descripcion_remisiones')
                    ->select('tabla_remisiones.*', 'tabla_descripcion_remisiones.*', 'tabla_clientes_remisiones.*', 'tabla_conductor_remision.*', 'tabla_productos_remisiones.*')
                    ->join('tabla_remisiones', 'tabla_detalle_descripcion_remisiones.id_remision_detalle', '=', 'tabla_remisiones.id_consecutivo')
                    ->join('tabla_descripcion_remisiones', 'tabla_detalle_descripcion_remisiones.id_descripcion_remisiones', '=', 'tabla_descripcion_remisiones.id_descripcion')
                    ->join('tabla_clientes_remisiones', 'tabla_remisiones.id_cliente', '=', 'tabla_clientes_remisiones.identificacion')
                    ->join('tabla_conductor_remision', 'tabla_remisiones.id_conductor_remision', '=', 'tabla_conductor_remision.identificacion_conductor')
                    ->join('tabla_productos_remisiones', 'tabla_descripcion_remisiones.id_producto', '=', 'tabla_productos_remisiones.id_producto')
                    ->paginate(10);
        */

        return view('remisiones.index', compact('remisiones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $remisiones = DB::table('tabla_remisiones')->where('estado', 1)->orderby('id_consecutivo','DESC')->take(1)->get();

        if(count($remisiones) >= 1) {
            
            foreach ($remisiones as $remision) {
                $codigo_temporal = strlen($remision->id_consecutivo);
                if($codigo_temporal == "1"){
                    $codigo_remision = "00"+$remision->id_consecutivo+1;
                    $numero_remision = "00"+$codigo_remision;        
                } else if($codigo_temporal == "2"){
                    $codigo_remision = "0"+$remision->id_consecutivo+1;
                    $numero_remision = "0"+$codigo_remision;
                } else {
                    $codigo_remision = $remision->id_consecutivo+1;
                    $numero_remision = $codigo_remision;
                }
            }

        }else{
            $numero_remision = '001';
        }

    
        $clientes = DB::table('tabla_clientes_remisiones')->where('estado_cliente', 1)->get();
        $productos = DB::table('tabla_productos_remisiones')->where('estado_producto', 1)->get();
        $conductores = DB::table('tabla_conductor_remision')->where('estado_conductor', 1)->get();
        $parametros = DB::table('tabla_parametros_remision')->where('estado_parametro', 1)->get();
        $tipo_identificacion = DB::table('tabla_tipo_identificacion')->where('estado_tipo', 1)->get();

        return view('remisiones.crear', compact('numero_remision', 'clientes', 'productos', 'conductores', 'parametros', 'tipo_identificacion'));
    }

    public function onchange_cliente($identificacion){

        $informacion_cliente = [];

        $clientes = DB::table('tabla_clientes_remisiones')
        ->where('estado_cliente', 1)
        ->where('identificacion', $identificacion)->get();

        foreach ($clientes as $cliente) {
            $informacion_cliente[] = $cliente;
        }

        return $informacion_cliente;
    }

    public function onchange_productos($valor){

        $informacion_producto = [];

        $productos = DB::table('tabla_productos_remisiones')
        ->where('id_producto', $valor)
        ->where('estado_producto', 1)->get();

        foreach ($productos as $producto) {
            $informacion_producto[] = $producto;
        }

        return $informacion_producto;
    }


    public function onchange_conductor($atributo, $informacion){

        $informacion_conductor = [];

        if($atributo == 'placa'){
            $informacion_sql = DB::table('tabla_conductor_remision')
            ->where('identificacion_conductor', $informacion)->get();
            

            foreach ($informacion_sql as $info) {
                $informacion_conductor[] = $info;
            }

        }else{

            $informacion_sql = DB::table('tabla_conductor_remision')
            ->where('identificacion_conductor', $informacion)->get();

            foreach ($informacion_sql as $info) {
                $informacion_conductor[] = $info;
            }
        }

        return $informacion_conductor;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //return $request->orden_remision;

        $request->validate([
            'codigo_remision' => 'required',
            'identificacion_cliente' => 'required',
            'fecha_remision' => 'required',
            'orden_remision' => 'required',
            'identificacion_conductor' => 'required'
        ]);
 

        $array_cantidad = ['hola'];
        $array_certificado = ['eliminar'];
        $array_certificado = ['eliminar'];
        $array_total_producto = ['eliminar'];


        foreach ($request->certificado_origen as $certificado) {
            $array_certificado[] = $certificado;    
        }
        unset($array_certificado[0]);


        foreach ($request->cantidad_producto as $cantidad) {
            $array_cantidad[] = $cantidad;    
        }
        unset($array_cantidad[0]);


        foreach ($request->total_producto as $total) {
            $array_total_producto[] = $total;    
        }
        unset($array_total_producto[0]);

        
        DB::table('tabla_remisiones')->insert([
            'id_consecutivo' => $request->codigo_remision,
            'id_cliente' => $request->identificacion_cliente,
            'fecha' => $request->fecha_remision,
            'vence' => $request->vence_remision,
            'orden' => $request->orden_remision,
            'id_conductor_remision' => $request->identificacion_conductor,
            'subtotal' => $request->subtotal_remision,
            'total' => $request->total_remision,
            'estado' => '1'
        ]);


        $i = 1;
        foreach ($request->codigo_producto as $producto) {
            $sum = $i++;
            if($producto != "0"){

                DB::table('tabla_descripcion_remisiones')->insert([
                    'id_producto' => $producto,
                    'certificado_de_origen' => $array_certificado[$sum],
                    'cantidad' => $array_cantidad[$sum],
                    'total' => $array_total_producto[$sum]
                ]);

                $tabla_detalle = DB::table('tabla_descripcion_remisiones')
                                    ->orderby('id_descripcion','DESC')->take(1)->get();

                foreach ($tabla_detalle as $valor) {
                    DB::table('tabla_detalle_descripcion_remisiones')->insert([
                        'id_remision_detalle' => $request->codigo_remision,
                        'id_descripcion_remisiones' => $valor->id_descripcion 
                    ]);
                }
                
            }else{
                
            }
        }

        return redirect()->route('remisiones.index')->with('status_success','Registro exitoso!'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $remision_unica = DB::table('tabla_remisiones')
                    ->select('tabla_clientes_remisiones.*', 'tabla_conductor_remision.*', 'tabla_remisiones.*')
                    ->join('tabla_clientes_remisiones', 'tabla_remisiones.id_cliente', '=', 'tabla_clientes_remisiones.identificacion')
                    ->join('tabla_conductor_remision', 'tabla_remisiones.id_conductor_remision', '=', 'tabla_conductor_remision.identificacion_conductor')
                    ->where('tabla_remisiones.id_consecutivo', $id)
                    ->paginate(10);

        $remisiones = DB::table('tabla_detalle_descripcion_remisiones')
                    ->select('tabla_remisiones.*', 'tabla_descripcion_remisiones.*', 'tabla_clientes_remisiones.*', 'tabla_conductor_remision.*', 'tabla_productos_remisiones.*')
                    ->join('tabla_remisiones', 'tabla_detalle_descripcion_remisiones.id_remision_detalle', '=', 'tabla_remisiones.id_consecutivo')
                    ->join('tabla_descripcion_remisiones', 'tabla_detalle_descripcion_remisiones.id_descripcion_remisiones', '=', 'tabla_descripcion_remisiones.id_descripcion')
                    ->join('tabla_clientes_remisiones', 'tabla_remisiones.id_cliente', '=', 'tabla_clientes_remisiones.identificacion')
                    ->join('tabla_conductor_remision', 'tabla_remisiones.id_conductor_remision', '=', 'tabla_conductor_remision.identificacion_conductor')
                    ->join('tabla_productos_remisiones', 'tabla_descripcion_remisiones.id_producto', '=', 'tabla_productos_remisiones.id_producto')
                    ->where('tabla_remisiones.id_consecutivo', $id)
                    ->paginate(10);

        return view('remisiones.show', compact('remisiones', 'remision_unica'));
    }

    public function exportar_pdf($id)
    {

        $parametros = DB::table('tabla_parametros_remision')->get();

        $remision_unica = DB::table('tabla_remisiones')
                    ->select('tabla_clientes_remisiones.*', 'tabla_conductor_remision.*', 'tabla_remisiones.*')
                    ->join('tabla_clientes_remisiones', 'tabla_remisiones.id_cliente', '=', 'tabla_clientes_remisiones.identificacion')
                    ->join('tabla_conductor_remision', 'tabla_remisiones.id_conductor_remision', '=', 'tabla_conductor_remision.identificacion_conductor')
                    ->where('tabla_remisiones.id_consecutivo', $id)
                    ->paginate(10);

        $remisiones = DB::table('tabla_detalle_descripcion_remisiones')
                    ->select('tabla_remisiones.*', 'tabla_descripcion_remisiones.*', 'tabla_clientes_remisiones.*', 'tabla_conductor_remision.*', 'tabla_productos_remisiones.*')
                    ->join('tabla_remisiones', 'tabla_detalle_descripcion_remisiones.id_remision_detalle', '=', 'tabla_remisiones.id_consecutivo')
                    ->join('tabla_descripcion_remisiones', 'tabla_detalle_descripcion_remisiones.id_descripcion_remisiones', '=', 'tabla_descripcion_remisiones.id_descripcion')
                    ->join('tabla_clientes_remisiones', 'tabla_remisiones.id_cliente', '=', 'tabla_clientes_remisiones.identificacion')
                    ->join('tabla_conductor_remision', 'tabla_remisiones.id_conductor_remision', '=', 'tabla_conductor_remision.identificacion_conductor')
                    ->join('tabla_productos_remisiones', 'tabla_descripcion_remisiones.id_producto', '=', 'tabla_productos_remisiones.id_producto')
                    ->where('tabla_remisiones.id_consecutivo', $id)
                    ->paginate(10);

        $pdf = PDF::loadView('remisiones.pdf', compact('remision_unica', 'remisiones', 'parametros'));

        return $pdf->download('registro-remisiones.pdf');
    }


    public function imprimir($id)
    {
        
        $parametros = DB::table('tabla_parametros_remision')->get();

        $remision_unica = DB::table('tabla_remisiones')
                    ->select('tabla_clientes_remisiones.*', 'tabla_conductor_remision.*', 'tabla_remisiones.*')
                    ->join('tabla_clientes_remisiones', 'tabla_remisiones.id_cliente', '=', 'tabla_clientes_remisiones.identificacion')
                    ->join('tabla_conductor_remision', 'tabla_remisiones.id_conductor_remision', '=', 'tabla_conductor_remision.identificacion_conductor')
                    ->where('tabla_remisiones.id_consecutivo', $id)
                    ->paginate(10);

        $remisiones = DB::table('tabla_detalle_descripcion_remisiones')
                    ->select('tabla_remisiones.*', 'tabla_descripcion_remisiones.*', 'tabla_clientes_remisiones.*', 'tabla_conductor_remision.*', 'tabla_productos_remisiones.*')
                    ->join('tabla_remisiones', 'tabla_detalle_descripcion_remisiones.id_remision_detalle', '=', 'tabla_remisiones.id_consecutivo')
                    ->join('tabla_descripcion_remisiones', 'tabla_detalle_descripcion_remisiones.id_descripcion_remisiones', '=', 'tabla_descripcion_remisiones.id_descripcion')
                    ->join('tabla_clientes_remisiones', 'tabla_remisiones.id_cliente', '=', 'tabla_clientes_remisiones.identificacion')
                    ->join('tabla_conductor_remision', 'tabla_remisiones.id_conductor_remision', '=', 'tabla_conductor_remision.identificacion_conductor')
                    ->join('tabla_productos_remisiones', 'tabla_descripcion_remisiones.id_producto', '=', 'tabla_productos_remisiones.id_producto')
                    ->where('tabla_remisiones.id_consecutivo', $id)
                    ->paginate(10);


        return view('remisiones.impresiones', compact('remisiones', 'remision_unica', 'parametros'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipo_identificacion = DB::table('tabla_tipo_identificacion')->where('estado_tipo', 1)->get();
        $parametros = DB::table('tabla_parametros_remision')->where('estado_parametro', 1)->get();
        $conductores = DB::table('tabla_conductor_remision')->where('estado_conductor', 1)->get();
        $informacion_producto = [];
        $informacion_conductor = [];
        $informacion_cliente = [];

        $remision_unica = DB::table('tabla_remisiones')
                    ->select('tabla_clientes_remisiones.*', 'tabla_conductor_remision.*', 'tabla_remisiones.*')
                    ->join('tabla_clientes_remisiones', 'tabla_remisiones.id_cliente', '=', 'tabla_clientes_remisiones.identificacion')
                    ->join('tabla_conductor_remision', 'tabla_remisiones.id_conductor_remision', '=', 'tabla_conductor_remision.identificacion_conductor')
                    ->where('tabla_remisiones.id_consecutivo', $id)
                    ->paginate(10);

        $clientes = DB::table('tabla_clientes_remisiones')->where('estado_cliente', 1)->get();
        $productos = DB::table('tabla_productos_remisiones')->where('estado_producto', 1)->get();

        $remisiones = DB::table('tabla_detalle_descripcion_remisiones')
                    ->select('tabla_remisiones.*', 'tabla_descripcion_remisiones.*', 'tabla_clientes_remisiones.*', 'tabla_conductor_remision.*', 'tabla_productos_remisiones.*', 'tabla_detalle_descripcion_remisiones.id AS id_detalle', 'tabla_detalle_descripcion_remisiones.id_remision_detalle', 'tabla_detalle_descripcion_remisiones.id_descripcion_remisiones')
                    ->join('tabla_remisiones', 'tabla_detalle_descripcion_remisiones.id_remision_detalle', '=', 'tabla_remisiones.id_consecutivo')
                    ->join('tabla_descripcion_remisiones', 'tabla_detalle_descripcion_remisiones.id_descripcion_remisiones', '=', 'tabla_descripcion_remisiones.id_descripcion')
                    ->join('tabla_clientes_remisiones', 'tabla_remisiones.id_cliente', '=', 'tabla_clientes_remisiones.identificacion')
                    ->join('tabla_conductor_remision', 'tabla_remisiones.id_conductor_remision', '=', 'tabla_conductor_remision.identificacion_conductor')
                    ->join('tabla_productos_remisiones', 'tabla_descripcion_remisiones.id_producto', '=', 'tabla_productos_remisiones.id_producto')
                    ->where('tabla_remisiones.id_consecutivo', $id)
                    ->paginate(10);
                    
        foreach ($remisiones as $code_producto) {
            $informacion_producto[] =  $code_producto->id_producto;
            $informacion_conductor[] =  $code_producto->id_conductor_remision;
            $informacion_cliente[] =  $code_producto->id_cliente;
            $codigo_remision =  $code_producto->id_consecutivo;
            $orden_remision =  $code_producto->orden;
            $fecha_remision = $code_producto->fecha;
            
        }

        return view('remisiones.edit', compact('tipo_identificacion', 'parametros', 'remisiones', 'remision_unica', 'productos', 'informacion_producto', 'conductores', 'informacion_conductor', 'clientes', 'informacion_cliente', 'codigo_remision', 'orden_remision', 'fecha_remision'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'codigo_remision' => 'required',
            'identificacion_cliente' => 'required',
            'fecha_remision' => 'required',
            'orden_remision' => 'required',
            'identificacion_conductor' => 'required'
        ]);

        $array_cantidad = ['hola'];
        $array_certificado = ['eliminar'];
        $array_certificado = ['eliminar'];
        $array_total_producto = ['eliminar'];


        foreach ($request->certificado_origen as $certificado) {
            $array_certificado[] = $certificado;    
        }
        unset($array_certificado[0]);


        foreach ($request->cantidad_producto as $cantidad) {
            $array_cantidad[] = $cantidad;    
        }
        unset($array_cantidad[0]);


        foreach ($request->total_producto as $total) {
            $array_total_producto[] = $total;    
        }
        unset($array_total_producto[0]);
        
        $remisiones = DB::table('tabla_remisiones')->where('id_consecutivo', $id)->delete();
        $descripcion_remision = DB::table('tabla_detalle_descripcion_remisiones')->where('id_remision_detalle', $id)->get();

        foreach ($descripcion_remision as $value) {
            DB::table('tabla_descripcion_remisiones')->where('id_descripcion', $value->id_descripcion_remisiones)->delete();
        }

        $descripcion_remision = DB::table('tabla_detalle_descripcion_remisiones')->where('id_remision_detalle', $id)->delete();


        /* registro nuevo */


        
        DB::table('tabla_remisiones')->insert([
            'id_consecutivo' => $request->codigo_remision,
            'id_cliente' => $request->identificacion_cliente,
            'fecha' => $request->fecha_remision,
            'vence' => $request->vence_remision,
            'orden' => $request->orden_remision,
            'id_conductor_remision' => $request->identificacion_conductor,
            'subtotal' => $request->subtotal_remision,
            'total' => $request->total_remision,
            'estado' => '1'
        ]);


        $i = 1;
        foreach ($request->codigo_producto as $producto) {
            $sum = $i++;
            if($producto != "0"){

                DB::table('tabla_descripcion_remisiones')->insert([
                    'id_producto' => $producto,
                    'certificado_de_origen' => $array_certificado[$sum],
                    'cantidad' => $array_cantidad[$sum],
                    'total' => $array_total_producto[$sum]
                ]);

                $tabla_detalle = DB::table('tabla_descripcion_remisiones')
                                    ->orderby('id_descripcion','DESC')->take(1)->get();

                foreach ($tabla_detalle as $valor) {
                    DB::table('tabla_detalle_descripcion_remisiones')->insert([
                        'id_remision_detalle' => $request->codigo_remision,
                        'id_descripcion_remisiones' => $valor->id_descripcion 
                    ]);
                }
                
            }else{
                
            }
        }

         return redirect()->route('remisiones.index')->with('status_success','Actualizacion exitosa!'); 


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('tabla_remisiones')->where('id_consecutivo', $id)->update([
            'estado' => '0',
        ]);

        return redirect()->route('remisiones.index')
            ->with('status_success','Borrado exitoso!'); 
    }
}
