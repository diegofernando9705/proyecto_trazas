<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Permisos\Models\Role;
use App\Permisos\Models\Permission;
use Illuminate\Support\Facades\Gate;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Exports\todoExport;
use Response;

class ReportesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        Gate::authorize('haveaccess', 'reportes.index');

        return view('reportes.index');
    }

    public function formulario($form) {
        //return $form;

        Gate::authorize('haveaccess', 'reportes.index');


        $regionales = DB::table('regionales')->get();

        $resultado = [];


        if ($form == 'todo') {

            $clave_conferencia = [];

            $valores = DB::table('accesos_historial')->select('conferencia')->get();

            foreach ($valores as $value) {
                $clave_conferencia[] = $value->conferencia;
            }

            $lista_simple = array_values(array_unique($clave_conferencia));

            return view('reportes.formularios.todo', compact('regionales', 'lista_simple'));
        } else if ($form == 'accesos') {

            $clave_conferencia = [];
            $valores = DB::table('accesos_historial')->select('conferencia')->get();

            foreach ($valores as $value) {
                $clave_conferencia[] = $value->conferencia;
            }

            $lista_simple = array_values(array_unique($clave_conferencia));

            return view('reportes.formularios.accesos', compact('regionales', 'lista_simple'));
        } else if ($form == 'mensajes') {

            return view('reportes.formularios.mensajes');
        } else {
            
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function generate(Request $request) {

        Gate::authorize('haveaccess', 'reportes.generate');

        $resultado = [];
        $reporte = [];
        $mje = [];


        if (empty($request['asociado'])) {
            $resultado_asociado = 0;
        } else {
            $resultado_asociado = count($request['asociado']);
        }


        if (empty($request['conferencia'])) {
            $resultado_conferencia = 0;
        } else {
            $resultado_conferencia = count($request['conferencia']);
        }

        if (!isset($request->fechaInicial) and ! isset($request->fechaFinal) and
                $resultado_conferencia == 0 and
                $resultado_asociado == 0) {


            $reportes = DB::table('accesos')
                    ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                    ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                    ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                    ->where('accesos.regional', '=', $request['regional'])
                    ->get();

            $mensajes = DB::table('mensajes')->get();

            foreach ($mensajes as $mensaje) {
                $mje[] = $mensaje->descripcion;
            }

            $frase = "Total de mensajes sin Fecha ni Conferencia: ";
            $total_mensajes = count($mje);

            $resultado[] = $reportes;
        } else {

            if (isset($request->fechaInicial) and
                    isset($request->fechaFinal) and
                    $resultado_conferencia == 0 and
                    $resultado_asociado == 0) {


                $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->whereBetween('accesos_historial.fecha', [$request->fechaInicial, $request->fechaFinal])
                        ->where('accesos.regional', '=', $request['regional'])
                        ->get();

                $resultado[] = $reportes;

                $mensajes = DB::table('mensajes')
                            ->whereBetween('fecha_mje', [$request->fechaInicial, $request->fechaFinal])
                            ->get();

            foreach ($mensajes as $mensaje) {
                $mje[] = $mensaje->descripcion;
            }

            $frase = "Total de mensajes sin Conferencia pero con Fecha: ";
            $total_mensajes = count($mje);

            $resultado[] = $reportes;




            } else if (isset($request->fechaInicial) and
                    isset($request->fechaFinal) and
                    $resultado_conferencia == 1 and
                    $resultado_asociado == 0) {

                $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->whereBetween('accesos_historial.fecha', [$request->fechaInicial, $request->fechaFinal])
                        ->where('accesos_historial.conferencia', '=', $request['conferencia'])
                        ->where('accesos.regional', '=', $request['regional'])
                        ->get();

                $resultado[] = $reportes;


                 $mensajes = DB::table('mensajes')
                            ->where('')
                            ->whereBetween('fecha_mje', [$request->fechaInicial, $request->fechaFinal])
                            ->get();

            foreach ($mensajes as $mensaje) {
                $mje[] = $mensaje->descripcion;
            }

            $frase = "Total de mensajes sin Conferencia pero con Fecha: ";
            $total_mensajes = count($mje);

            $resultado[] = $reportes;




            } else if (isset($request->fechaInicial) and
                    isset($request->fechaFinal) and
                    $resultado_conferencia > 1 and
                    $resultado_asociado == 0) {


                foreach ($request['conferencia'] as $conferencia) {

                    $reportes = DB::table('accesos')
                            ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                            ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                            ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                            ->whereBetween('accesos_historial.fecha', [$request->fechaInicial, $request->fechaFinal])
                            ->where('accesos.regional', '=', $request['regional'])
                            ->where('accesos_historial.conferencia', '=', $conferencia)
                            ->get();

                    $resultado[] = $reportes;
                }
            } else if (isset($request->fechaInicial) and
                    isset($request->fechaFinal) and
                    $resultado_conferencia == 1 and
                    $resultado_asociado == 1) {


                $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->whereBetween('accesos_historial.fecha', [$request->fechaInicial, $request->fechaFinal])
                        ->where('accesos.regional', '=', $request['regional'])
                        ->where('accesos.asociado', '=', $request['asociado'])
                        ->where('accesos_historial.conferencia', '=', $request['conferencia'])
                        ->get();

                $resultado[] = $reportes;
                
                
            } else if (isset($request->fechaInicial) and
                    isset($request->fechaFinal) and
                    $resultado_conferencia > 1 and
                    $resultado_asociado > 1) {

                foreach ($request['conferencia'] as $conferencia) {

                    $reportes = DB::table('accesos')
                            ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                            ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                            ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                            ->whereBetween('accesos_historial.fecha', [$request->fechaInicial, $request->fechaFinal])
                            ->where('accesos.regional', '=', $request['regional'])
                            ->where('accesos_historial.conferencia', '=', $conferencia)
                            ->get();

                    $resultado[] = $reportes;
                }
                
                
            } else if (isset($request->fechaInicial) and
                    isset($request->fechaFinal) and
                    $resultado_conferencia == 0 and
                    $resultado_asociado == 1) {

                    $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->whereBetween('accesos_historial.fecha', [$request->fechaInicial, $request->fechaFinal])
                        ->where('accesos.regional', '=', $request['regional'])
                        ->where('accesos.asociado', '=', $request['asociado'])
                        ->get();

                    $resultado[] = $reportes;
                
            } else if  (isset($request->fechaInicial) and
                    isset($request->fechaFinal) and
                    $resultado_conferencia == 0 and
                    $resultado_asociado > 1) {


                $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->whereBetween('accesos_historial.fecha', [$request->fechaInicial, $request->fechaFinal])
                        ->where('accesos.regional', '=', $request['regional'])
                        ->get();

                $resultado[] = $reportes;

                
            } else if (!isset($request->fechaInicial) and !isset($request->fechaFinal) and
                    $resultado_conferencia == 1 and
                    $resultado_asociado == 0) {

                $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->where('accesos.regional', '=', $request['regional'])
                        ->where('accesos_historial.conferencia', '=', $request['conferencia'])
                        ->get();

                $resultado[] = $reportes;
                
            } else if (!isset($request->fechaInicial) and !isset($request->fechaFinal) and
                    $resultado_conferencia == 1 and
                    $resultado_asociado == 1) {

                $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->where('accesos.asociado', '=', $request['asociado'])
                        ->where('accesos_historial.conferencia', '=', $request['conferencia'])
                        ->get();

                $resultado[] = $reportes;
                
            } else if (!isset($request->fechaInicial) and !isset($request->fechaFinal) and
                    $resultado_conferencia == 1 and
                    $resultado_asociado == 1) {

                $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->where('accesos.asociado', '=', $request['asociado'])
                        ->where('accesos_historial.conferencia', '=', $request['conferencia'])
                        ->get();

                    $resultado[] = $reportes;
                    
            } else if (!isset($request->fechaInicial) and !isset($request->fechaFinal) and
                    $resultado_conferencia > 1 and
                    $resultado_asociado > 1) {

                foreach ($request['conferencia'] as $conferencia) {

                    $reportes = DB::table('accesos')
                            ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                            ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                            ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                            ->where('accesos.regional', '=', $request['regional'])
                            ->where('accesos_historial.conferencia', '=', $conferencia)
                            ->get();

                    $resultado[] = $reportes;
                }
                    
            } else if (!isset($request->fechaInicial) and !isset($request->fechaFinal) and
                    $resultado_conferencia == 0 and
                    $resultado_asociado == 1) {


                    $reportes = DB::table('accesos')
                            ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                            ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                            ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                            ->where('accesos.regional', '=', $request['regional'])
                            ->where('accesos.asociado', '=', $request['asociado'])
                            ->get();

                    $resultado[] = $reportes;
          
            } else if (!isset($request->fechaInicial) and !isset($request->fechaFinal) and
                    $resultado_conferencia > 1 and
                    $resultado_asociado == 0) {


                   foreach ($request['conferencia'] as $conferencia) {

                    $reportes = DB::table('accesos')
                            ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                            ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                            ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                            ->where('accesos.regional', '=', $request['regional'])
                            ->where('accesos_historial.conferencia', '=', $conferencia)
                            ->get();

                    $resultado[] = $reportes;
                }
                
            } else {
                
            }
        }

        return view('reportes.formularios.respuesta.todo', compact('resultado', 'total_mensajes', 'frase'));
    }

    /*
     *
     *
     * *
     * *
     * *
     * 
     */

    // ================================================== EXPORTACION TABLAS UNIFICADA ==================================================

    public function exportarCSVTodo(Request $request) {

        $resultado = [];
        $reporte = [];
        
        $array_conferencia = [];
        
        if (empty($request['asociadoCSV'][0])) {
            $resultado_asociado = 0;
        } else {
            $contenido_asociado = explode(",", $request['asociadoCSV'][0]);
            $array_asociado = [];
            
            foreach ($contenido_asociado as $asociado){
                $array_asociado[] = $asociado;
            };
            
            $resultado_asociado = count($array_asociado);
        }


        if (empty($request['conferenciaCSV'][0])) {
            $resultado_conferencia = 0;
        } else {
            $contenido_conferencia = explode(",", $request['conferenciaCSV'][0]);
            
            
            foreach ($contenido_conferencia as $conferencia){
                $array_conferencia[] = $conferencia;
            };
            
            $resultado_conferencia = count($array_conferencia);
        }
        
        if (!isset($request->fechaInicialCSV) and ! isset($request->fechaFinalCSV) and
                $resultado_conferencia == 0 and
                $resultado_asociado == 0) {


            $reportes = DB::table('accesos')
                    ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                    ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                    ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                    ->where('accesos.regional', '=', $request['regionalCSV'])
                    ->get();


            $resultado[] = $reportes;
        } else {

            if (isset($request->fechaInicialCSV) and
                    isset($request->fechaFinalCSV) and
                    $resultado_conferencia == 0 and
                    $resultado_asociado == 0) {


                $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->whereBetween('accesos_historial.fecha', [$request->fechaInicialCSV, $request->fechaFinalCSV])
                        ->where('accesos.regional', '=', $request['regionalCSV'])
                        ->get();

                $resultado[] = $reportes;
                
            } else if (isset($request->fechaInicialCSV) and
                    isset($request->fechaFinalCSV) and
                    $resultado_conferencia == 1 and
                    $resultado_asociado == 0) {

                $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->whereBetween('accesos_historial.fecha', [$request->fechaInicialCSV, $request->fechaFinalCSV])
                        ->where('accesos_historial.conferencia', '=', $request['conferencia'])
                        ->where('accesos.regional', '=', $request['regionalCSV'])
                        ->get();

                $resultado[] = $reportes;
            } else if (isset($request->fechaInicialCSV) and
                    isset($request->fechaFinalCSV) and
                    $resultado_conferencia > 1 and
                    $resultado_asociado == 0) {


                foreach ($request['conferenciaCSV'] as $conferencia) {

                    $reportes = DB::table('accesos')
                            ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                            ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                            ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                            ->whereBetween('accesos_historial.fecha', [$request->fechaInicialCSV, $request->fechaFinalCSV])
                            ->where('accesos.regional', '=', $request['regionalCSV'])
                            ->where('accesos_historial.conferencia', '=', $conferencia)
                            ->get();

                    $resultado[] = $reportes;
                }
            } else if (isset($request->fechaInicialCSV) and
                    isset($request->fechaFinalCSV) and
                    $resultado_conferencia == 1 and
                    $resultado_asociado == 1) {


                $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->whereBetween('accesos_historial.fecha', [$request->fechaInicialCSV, $request->fechaFinalCSV])
                        ->where('accesos.regional', '=', $request['regionalCSV'])
                        ->where('accesos.asociado', '=', $request['asociadoCSV'])
                        ->where('accesos_historial.conferencia', '=', $request['conferenciaCSV'])
                        ->get();

                $resultado[] = $reportes;
                
                
            } else if (isset($request->fechaInicialCSV) and
                    isset($request->fechaFinalCSV) and
                    $resultado_conferencia > 1 and
                    $resultado_asociado > 1) {
                
                foreach ($array_conferencia as $conferencia) {
                    
                    
                    $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->whereBetween('accesos_historial.fecha', [$request->fechaInicialCSV, $request->fechaFinalCSV])
                        ->where('accesos.regional', '=', $request['regionalCSV'])
                        ->where('accesos_historial.conferencia', '=', $conferencia)
                        ->get();
                    
                    $resultado[] = $reportes;
                }
                
            } else if (isset($request->fechaInicialCSV) and
                    isset($request->fechaFinalCSV) and
                    $resultado_conferencia == 0 and
                    $resultado_asociado == 1) {

                    $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->whereBetween('accesos_historial.fecha', [$request->fechaInicialCSV, $request->fechaFinalCSV])
                        ->where('accesos.regional', '=', $request['regionalCSV'])
                        ->where('accesos.asociado', '=', $request['asociado'])
                        ->get();

                    $resultado[] = $reportes;
                
            } else if  (isset($request->fechaInicialCSV) and
                    isset($request->fechaFinalCSV) and
                    $resultado_conferencia == 0 and
                    $resultado_asociado > 1) {


                $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->whereBetween('accesos_historial.fecha', [$request->fechaInicialCSV, $request->fechaFinalCSV])
                        ->where('accesos.regional', '=', $request['regionalCSV'])
                        ->get();

                $resultado[] = $reportes;

                
            } else if (!isset($request->fechaInicialCSV) and !isset($request->fechaFinalCSV) and
                    $resultado_conferencia == 1 and
                    $resultado_asociado == 0) {

                $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->where('accesos.regional', '=', $request['regionalCSV'])
                        ->where('accesos_historial.conferencia', '=', $request['conferenciaCSV'])
                        ->get();

                $resultado[] = $reportes;
                
            } else if (!isset($request->fechaInicialCSV) and !isset($request->fechaFinalCSV) and
                    $resultado_conferencia == 1 and
                    $resultado_asociado == 1) {

                $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->where('accesos.asociado', '=', $request['asociado'])
                        ->where('accesos_historial.conferencia', '=', $request['conferenciaCSV'])
                        ->get();

                $resultado[] = $reportes;
                
            } else if (!isset($request->fechaInicialCSV) and !isset($request->fechaFinalCSV) and
                    $resultado_conferencia == 1 and
                    $resultado_asociado == 1) {

                $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->where('accesos.asociado', '=', $request['asociado'])
                        ->where('accesos_historial.conferencia', '=', $request['conferenciaCSV'])
                        ->get();

                    $resultado[] = $reportes;
                    
            } else if (!isset($request->fechaInicialCSV) and !isset($request->fechaFinalCSV) and
                    $resultado_conferencia > 1 and
                    $resultado_asociado > 1) {

                foreach ($array_conferencia as $conferencia) {

                    $reportes = DB::table('accesos')
                            ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                            ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                            ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                            ->where('accesos.regional', '=', $request['regionalCSV'])
                            ->where('accesos_historial.conferencia', '=', $conferencia)
                            ->get();

                    $resultado[] = $reportes;
                }
                    
            } else if (!isset($request->fechaInicialCSV) and !isset($request->fechaFinalCSV) and
                    $resultado_conferencia == 0 and
                    $resultado_asociado == 1) {


                    $reportes = DB::table('accesos')
                            ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                            ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                            ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                            ->where('accesos.regional', '=', $request['regionalCSV'])
                            ->where('accesos.asociado', '=', $request['asociado'])
                            ->get();

                    $resultado[] = $reportes;
          
            } else if (!isset($request->fechaInicialCSV) and !isset($request->fechaFinalCSV) and
                    $resultado_conferencia > 1 and
                    $resultado_asociado == 0) {


                   foreach ($array_conferencia as $conferencia) {

                    $reportes = DB::table('accesos')
                            ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                            ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                            ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                            ->where('accesos.regional', '=', $request['regionalCSV'])
                            ->where('accesos_historial.conferencia', '=', $conferencia)
                            ->get();

                    $resultado[] = $reportes;
                }
                
            } else {
                
            }
        }
        
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
        
        //$total_resultado = sizeof($resultado);
        //return $total_resultado;

        $columns = array('Identificacion', 'Nombre', 'Fecha', 'Descripcion mensaje', 'Regional', 'Nombre conferencia', 'Asociado/No Asociado');

        $callback = function() use ($resultado, $columns) {
            $i = 0;
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($resultado as $value) {
                foreach ($value as $register) {

                    $i++;

                    if ($register->asociado == 1) {
                        $asociado = "Asociado";
                    } else {
                        $asociado = "No asociado";
                    }

                    fputcsv($file, array($register->identificacion, $register->nombre, $register->fecha, $register->descripcion, $register->regional, $register->conferencia, $asociado));
                }
            }


            $columnTotal = array(
                array(),
                array('Total Registros', $i),
                array('Total Mensajes', ""));

            foreach ($columnTotal as $valor) {
                fputcsv($file, $valor);
            }



            fclose($file);
        };

        return response()->streamDownload($callback, 'prefix-' . date('d-m-Y-H:i:s') . '.csv', $headers);


        // var_dump($resultado);
    }

    public function exportarPDFTodo(Request $request) {
        Gate::authorize('haveaccess', 'reportes.exportTodoPdf');

        $resultado = [];
        $reporte = [];
        $array_conferencia = [];
        
        if (empty($request['asociadoPDF'][0])) {
            $resultado_asociado = 0;
        } else {
            $contenido_asociado = explode(",", $request['asociadoPDF'][0]);
            $array_asociado = [];
            
            foreach ($contenido_asociado as $asociado){
                $array_asociado[] = $asociado;
            };
            
            $resultado_asociado = count($array_asociado);
        }


        if (empty($request['conferenciaPDF'][0])) {
            $resultado_conferencia = 0;
        } else {
            $contenido_conferencia = explode(",", $request['conferenciaPDF'][0]);
            
            
            foreach ($contenido_conferencia as $conferencia){
                $array_conferencia[] = $conferencia;
            };
            
            $resultado_conferencia = count($array_conferencia);
        }
        
        if (!isset($request->fechaInicialPDF) and ! isset($request->fechaFinalPDF) and
                $resultado_conferencia == 0 and
                $resultado_asociado == 0) {


            $reportes = DB::table('accesos')
                    ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                    ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                    ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                    ->where('accesos.regional', '=', $request['regionalPDF'])
                    ->get();


            $resultado[] = $reportes;
        } else {

            if (isset($request->fechaInicialPDF) and
                    isset($request->fechaFinalPDF) and
                    $resultado_conferencia == 0 and
                    $resultado_asociado == 0) {


                $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->whereBetween('accesos_historial.fecha', [$request->fechaInicialPDF, $request->fechaFinalPDF])
                        ->where('accesos.regional', '=', $request['regionalPDF'])
                        ->get();

                $resultado[] = $reportes;
                
            } else if (isset($request->fechaInicialPDF) and
                    isset($request->fechaFinalPDF) and
                    $resultado_conferencia == 1 and
                    $resultado_asociado == 0) {

                $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->whereBetween('accesos_historial.fecha', [$request->fechaInicialPDF, $request->fechaFinalPDF])
                        ->where('accesos_historial.conferencia', '=', $request['conferencia'])
                        ->where('accesos.regional', '=', $request['regionalPDF'])
                        ->get();

                $resultado[] = $reportes;
            } else if (isset($request->fechaInicialPDF) and
                    isset($request->fechaFinalPDF) and
                    $resultado_conferencia > 1 and
                    $resultado_asociado == 0) {


                foreach ($request['conferenciaPDF'] as $conferencia) {

                    $reportes = DB::table('accesos')
                            ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                            ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                            ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                            ->whereBetween('accesos_historial.fecha', [$request->fechaInicialPDF, $request->fechaFinalPDF])
                            ->where('accesos.regional', '=', $request['regionalPDF'])
                            ->where('accesos_historial.conferencia', '=', $conferencia)
                            ->get();

                    $resultado[] = $reportes;
                }
            } else if (isset($request->fechaInicialPDF) and
                    isset($request->fechaFinalPDF) and
                    $resultado_conferencia == 1 and
                    $resultado_asociado == 1) {


                $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->whereBetween('accesos_historial.fecha', [$request->fechaInicialPDF, $request->fechaFinalPDF])
                        ->where('accesos.regional', '=', $request['regionalPDF'])
                        ->where('accesos.asociado', '=', $request['asociadoPDF'])
                        ->where('accesos_historial.conferencia', '=', $request['conferenciaPDF'])
                        ->get();

                $resultado[] = $reportes;
                
                
            } else if (isset($request->fechaInicialPDF) and
                    isset($request->fechaFinalPDF) and
                    $resultado_conferencia > 1 and
                    $resultado_asociado > 1) {
                
                foreach ($array_conferencia as $conferencia) {
                    
                    
                    $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->whereBetween('accesos_historial.fecha', [$request->fechaInicialPDF, $request->fechaFinalPDF])
                        ->where('accesos.regional', '=', $request['regionalPDF'])
                        ->where('accesos_historial.conferencia', '=', $conferencia)
                        ->get();
                    
                    $resultado[] = $reportes;
                }
                
            } else if (isset($request->fechaInicialPDF) and
                    isset($request->fechaFinalPDF) and
                    $resultado_conferencia == 0 and
                    $resultado_asociado == 1) {

                    $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->whereBetween('accesos_historial.fecha', [$request->fechaInicialPDF, $request->fechaFinalPDF])
                        ->where('accesos.regional', '=', $request['regionalPDF'])
                        ->where('accesos.asociado', '=', $request['asociadoPDF'])
                        ->get();

                    $resultado[] = $reportes;
                
            } else if  (isset($request->fechaInicialPDF) and
                    isset($request->fechaFinalPDF) and
                    $resultado_conferencia == 0 and
                    $resultado_asociado > 1) {


                $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->whereBetween('accesos_historial.fecha', [$request->fechaInicialPDF, $request->fechaFinalPDF])
                        ->where('accesos.regional', '=', $request['regionalPDF'])
                        ->get();

                $resultado[] = $reportes;

                
            } else if (!isset($request->fechaInicialPDF) and !isset($request->fechaFinalPDF) and
                    $resultado_conferencia == 1 and
                    $resultado_asociado == 0) {

                $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->where('accesos.regional', '=', $request['regionalPDF'])
                        ->where('accesos_historial.conferencia', '=', $request['conferenciaPDF'])
                        ->get();

                $resultado[] = $reportes;
                
            } else if (!isset($request->fechaInicialPDF) and !isset($request->fechaFinalPDF) and
                    $resultado_conferencia == 1 and
                    $resultado_asociado == 1) {

                $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->where('accesos.asociado', '=', $request['asociadoPDF'])
                        ->where('accesos_historial.conferencia', '=', $request['conferenciaPDF'])
                        ->get();

                $resultado[] = $reportes;
                
            } else if (!isset($request->fechaInicialPDF) and !isset($request->fechaFinalPDF) and
                    $resultado_conferencia == 1 and
                    $resultado_asociado == 1) {

                $reportes = DB::table('accesos')
                        ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                        ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                        ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                        ->where('accesos.asociado', '=', $request['asociadoPDF'])
                        ->where('accesos_historial.conferencia', '=', $request['conferenciaPDF'])
                        ->get();

                    $resultado[] = $reportes;
                    
            } else if (!isset($request->fechaInicialPDF) and !isset($request->fechaFinalPDF) and
                    $resultado_conferencia > 1 and
                    $resultado_asociado > 1) {

                foreach ($array_conferencia as $conferencia) {

                    $reportes = DB::table('accesos')
                            ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                            ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                            ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                            ->where('accesos.regional', '=', $request['regionalPDF'])
                            ->where('accesos_historial.conferencia', '=', $conferencia)
                            ->get();

                    $resultado[] = $reportes;
                }
                    
            } else if (!isset($request->fechaInicialPDF) and !isset($request->fechaFinalPDF) and
                    $resultado_conferencia == 0 and
                    $resultado_asociado == 1) {


                    $reportes = DB::table('accesos')
                            ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                            ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                            ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                            ->where('accesos.regional', '=', $request['regionalCSV'])
                            ->where('accesos.asociado', '=', $request['asociado'])
                            ->get();

                    $resultado[] = $reportes;
          
            } else if (!isset($request->fechaInicialPDF) and !isset($request->fechaFinalPDF) and
                    $resultado_conferencia > 1 and
                    $resultado_asociado == 0) {


                   foreach ($array_conferencia as $conferencia) {

                    $reportes = DB::table('accesos')
                            ->select('accesos.*', 'accesos_historial.*', 'mensajes.*')
                            ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion_fk')
                            ->join('mensajes', 'accesos_historial.code_acceso', '=', 'mensajes.code_acceso_fk')
                            ->where('accesos.regional', '=', $request['regionalPDF'])
                            ->where('accesos_historial.conferencia', '=', $conferencia)
                            ->get();

                    $resultado[] = $reportes;
                }
                
            } else {
                
            }
        }

        $pdf = \PDF::loadView('reportes.formularios.export.pdf-todo', compact('resultado'));
        return $pdf->download('sistema-de-reportes-softworld.pdf');
    }

    // ================================================== FIN EXPORTACION TABLAS UNIFICADA ==================================================


    /*
     *
     *
     * *
     * *
     * *
     * 
     */

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function tableacceso(Request $request) {



        if (!empty($request->nombre) or ! empty($request->apellido) or ! empty($request->correo)) {

            if (empty($request['asociado']) or count($request['asociado']) > 1) {
                $resultado = DB::table('accesos')
                                ->select('regionales.nombre as nombreRegional', 'regionales.estado as estadoRegional', 'accesos.*')
                                ->join('regionales', 'accesos.regional', '=', 'regionales.id')
                                ->where('accesos.regional', '=', $request->regional)
                                ->where('accesos.email', 'like', '%' . $request->correo . '%')
                                ->where('accesos.apellido', 'like', '%' . $request->apellido . '%')
                                ->where('accesos.nombre', 'like', '%' . $request->nombre . '%')->get();
            } else {

                $resultado = DB::table('accesos')
                                ->select('regionales.nombre as nombreRegional', 'regionales.estado as estadoRegional', 'accesos.*')
                                ->join('regionales', 'accesos.regional', '=', 'regionales.id')
                                ->where('accesos.regional', '=', $request->regional)
                                ->where('accesos.asociado', '=', $request['asociado'])
                                ->where('accesos.email', 'like', '%' . $request->correo . '%')
                                ->where('accesos.apellido', 'like', '%' . $request->apellido . '%')
                                ->where('accesos.nombre', 'like', '%' . $request->nombre . '%')->get();
            }

            return view('reportes.formularios.respuesta.accesos', compact('resultado'));
        }
    }

    /*
     *
     *
     * *
     * *
     * *
     * 
     */

    // ================================================== EXPORTACION TABLA ACCESOS ==================================================
    public function generateAccesos(Request $request) {

        $resultado = [];

        if ($request['asociadoPDF'] == "0" or $request['asociadoPDF'] == NULL or empty($request['asociadoPDF'])) {
            $asociado_validate = $request['asociadoPDF'];
        } else {
            if ($request['asociadoPDF'] == "1" or $request['asociadoPDF'] == "2") {
                $asociado_validate = $request['asociadoPDF'];
            } else {
                $asociado_validate = "3";
            }
        }
        print_r($request);
        return $request;
        if ($asociado_validate == "3" or $asociado_validate == "0") {

            if (empty($request['nombrePDF']) or empty($request['apellidoPDF']) or empty($request['correoPDF'])) {

                $reportes = DB::table('accesos')
                                ->select('regionales.nombre as nombreRegional', 'regionales.estado as estadoRegional', 'accesos.*')
                                ->join('regionales', 'accesos.regional', '=', 'regionales.id')
                                ->where('accesos.regional', '=', $request["regionalPDF"])->get();
                $resultado[] = $reportes;

                return $resultado;
            } else if (empty($request['apellidoPDF']) or empty($request['correoPDF']) and ! empty($request['nombrePDF'])) {

                $reportes = DB::table('accesos')
                                ->select('regionales.nombre as nombreRegional', 'regionales.estado as estadoRegional', 'accesos.*')
                                ->join('regionales', 'accesos.regional', '=', 'regionales.id')
                                ->where('accesos.nombre', 'like', '%' . $request['nombrePDF'] . '%')
                                ->where('accesos.regional', '=', $request["regionalPDF"])->get();
                $resultado[] = $reportes;
                return "bien";
            }
        } else {

            $reportes = DB::table('accesos')
                            ->select('regionales.nombre as nombreRegional', 'regionales.estado as estadoRegional', 'accesos.*')
                            ->join('regionales', 'accesos.regional', '=', 'regionales.id')
                            ->where('accesos.regional', '=', $request["regionalPDF"])
                            ->where('accesos.email', 'like', '%' . $request["correoPDF"] . '%')
                            ->where('accesos.apellido', 'like', '%' . $request["apellidoPDF"] . '%')
                            ->where('accesos.nombre', 'like', '%' . $request["nombrePDF"] . '%')->get();

            $resultado[] = $reportes;
            var_dump($resultado);
            return $resultado;
        }

        return view('reportes.formularios.respuesta.accesos', compact('resultado'));
    }

    public function exportarCSVAccesos(Request $request) {

        Gate::authorize('haveaccess', 'reportes.export');


        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        if ($request['asociadoCSV'][0] == "0") {
            $asociado_validate = 0;
        } else {
            if ($request['asociadoCSV'][0] == "1" or $request['asociadoCSV'][0] == "2") {
                $asociado_validate = $request['asociadoCSV'][0];
            } else {
                $asociado_validate = "3";
            }
        }

        if ($request['asociadoCSV'][0] == "1" or $request['asociadoCSV'][0] == "2") {

            $reportes = DB::table('accesos')
                            ->select('regionales.nombre as nombreRegional', 'regionales.estado as estadoRegional', 'accesos.*')
                            ->join('regionales', 'accesos.regional', '=', 'regionales.id')
                            ->where('accesos.regional', '=', $request["regionalCSV"])
                            ->where('accesos.asociado', '=', $asociado_validate)
                            ->where('accesos.email', 'like', '%' . $request["correoCSV"] . '%')
                            ->where('accesos.apellido', 'like', '%' . $request["apellidoCSV"] . '%')
                            ->where('accesos.nombre', 'like', '%' . $request["nombreCSV"] . '%')->get();

            $resultado[] = $reportes;
        } else {

            $reportes = DB::table('accesos')
                            ->select('regionales.nombre as nombreRegional', 'regionales.estado as estadoRegional', 'accesos.*')
                            ->join('regionales', 'accesos.regional', '=', 'regionales.id')
                            ->where('accesos.regional', '=', $request["regionalCSV"])
                            ->where('accesos.email', 'like', '%' . $request["correoCSV"] . '%')
                            ->where('accesos.apellido', 'like', '%' . $request["apellidoCSV"] . '%')
                            ->where('accesos.nombre', 'like', '%' . $request["nombreCSV"] . '%')->get();

            $resultado[] = $reportes;
        }

        $columns = array('Identificacion', 'Nombre', 'Apellido', 'Correo', 'Telefono', 'Celular', 'Asociado/No Asociado', 'rol_secretaria');

        $callback = function() use ($resultado, $columns) {
            $i = 0;
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($resultado as $value) {
                foreach ($value as $register) {
                    $i++;
                    if ($register->asociado == 1) {
                        $asociado = "Asociado";
                    } else {
                        $asociado = "No asociado";
                    }

                    fputcsv($file, array($register->identificacion, $register->nombre, $register->apellido, $register->email, $register->telefono, $register->celular, $asociado, $register->rol_secretaria));
                }
            }

            fclose($file);
        };

        return response()->streamDownload($callback, 'prefix-' . date('d-m-Y-H:i:s') . '.csv', $headers);
    }

    public function exportarPDFAccesos(Request $request) {

        Gate::authorize('haveaccess', 'reportes.exportpdf');

        if ($request['asociadoPDF'] == "0" or $request['asociadoPDF'] == NULL or empty($request['asociadoPDF'])) {
            $asociado_validate = $request['asociadoPDF'];
        } else {
            if ($request['asociadoPDF'] == "1" or $request['asociadoPDF'] == "2") {
                $asociado_validate = $request['asociadoPDF'];
            } else {
                $asociado_validate = "3";
            }
        }


        if ($asociado_validate == "3") {
            $reportes = DB::table('accesos')
                            ->select('regionales.nombre as nombreRegional', 'regionales.estado as estadoRegional', 'accesos.*')
                            ->join('regionales', 'accesos.regional', '=', 'regionales.id')
                            ->where('accesos.regional', '=', $request["regionalCSV"])
                            ->where('accesos.email', 'like', '%' . $request["correoCSV"] . '%')
                            ->where('accesos.apellido', 'like', '%' . $request["apellidoCSV"] . '%')
                            ->where('accesos.nombre', 'like', '%' . $request["nombreCSV"] . '%')->get();

            $resultado[] = $reportes;
        } else {
            $reportes = DB::table('accesos')
                            ->select('regionales.nombre as nombreRegional', 'regionales.estado as estadoRegional', 'accesos.*')
                            ->join('regionales', 'accesos.regional', '=', 'regionales.id')
                            ->where('accesos.regional', '=', $request["regionalCSV"])
                            ->where('accesos.asociado', '=', $asociado_validate)
                            ->where('accesos.email', 'like', '%' . $request["correoCSV"] . '%')
                            ->where('accesos.apellido', 'like', '%' . $request["apellidoCSV"] . '%')
                            ->where('accesos.nombre', 'like', '%' . $request["nombreCSV"] . '%')->get();

            $resultado[] = $reportes;
        }


        $pdf = \PDF::loadView('reportes.formularios.export.pdf', compact('resultado'));
        return $pdf->download('sistema-de-reportes-softworld.pdf');
    }

    // ================================================== FIN EXPORTACION TABLA ACCESOS ==================================================



    public function generatemensajes(Request $request) {

        $resultado = DB::table('mensajes')->whereBetween('fecha_ingreso', [$request->fechaInicial, $request->fechaFinal])->get();
        $totales = DB::table('mensajes')->whereBetween('fecha_ingreso', [$request->fechaInicial, $request->fechaFinal])->count();

        return view('reportes.formularios.respuesta.mensajes', compact('resultado', 'totales'));
    }

    public function exportMensajesCSV(Request $request) {

        Gate::authorize('haveaccess', 'reportes.export.mensajes');

        //return $request->nombre;
        //  return Excel::download(new TableaccesoExport($request->nombre), 'invoices.xlsx');
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $resultado = DB::table('mensajes')->whereBetween('fecha_ingreso', [$request->fechaInicial, $request->fechaFinal])->get();
        $totales = DB::table('mensajes')->whereBetween('fecha_ingreso', [$request->fechaInicial, $request->fechaFinal])->count();




        $columns = array('Identificacion', 'Nombre', 'Descripcion', 'Conferencia');

        $callback = function() use ($resultado, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($resultado as $register) {
                fputcsv($file, array($register->identificacion, $register->nombre, $register->descripcion, $register->conferencia));
            }
            fclose($file);
        };

        return response()->streamDownload($callback, 'prefix-' . date('d-m-Y-H:i:s') . '.csv', $headers);
    }

    public function exportMensajesPDF(Request $request) {
        Gate::authorize('haveaccess', 'reportes.export.mensajespdf');

        $resultado = DB::table('mensajes')->whereBetween('fecha_ingreso', [$request->fechaInicial, $request->fechaFinal])->get();
        $totales = DB::table('mensajes')->whereBetween('fecha_ingreso', [$request->fechaInicial, $request->fechaFinal])->count();


        $pdf = \PDF::loadView('reportes.formularios.export.pdf-mensajes', compact('resultado', 'totales'));
        return $pdf->download('sistema-de-reportes-softworld.pdf');
    }

    /*
     *
     *
     * *
     * *
     * *
     * 
     */
}
