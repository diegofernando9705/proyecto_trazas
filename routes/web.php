<?php

use Illuminate\Support\Facades\Route;
use App\User;
use App\Permisos\Models\Role;
use App\Permisos\Models\Permission;
use Illuminate\Support\Facades\Gate;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
  if(Auth::guest())
    return view('auth.login');                   
  else
    return view('home');

});

Route::get('/remisiones', function () {
  return view('home_remisiones');
});


Auth::routes();

Route::get('/logout', function () {
  Auth::logout();
  return redirect('/');
});

Route::get('/home', 'HomeController@index')->name('home');


Route::resource('/role', 'RoleController')->names('role');

Route::resource('/remisiones/remisiones', 'RemisionesController')->names('remisiones');

Route::resource('/remisiones/clientes', 'ClientesRemisionController')->names('clientesRemisiones');
Route::resource('/remisiones/productos', 'ProductosRemisionController')->names('productosRemisiones');
Route::resource('/remisiones/conductores', 'ConductoresRemisionController')->names('conductoresRemisiones');
Route::resource('/remisiones/parametros', 'ParametrosRemisionController')->names('parametrosRemisiones');



Route::get('/remisiones/keyup/clientes/{valor?}', 'ClientesRemisionController@keyup_cliente');
Route::get('/remisiones/keyup/productos/{valor?}', 'ProductosRemisionController@keyup_producto');
Route::get('/remisiones/keyup/conductores/{valor?}', 'ConductoresRemisionController@keyup_conductores');

/* relleno de informacion en formulario remisiones segun cliente*/
Route::get('/remisiones/onchange/cliente/{valor?}', 'RemisionesController@onchange_cliente');

Route::get('/remisiones/onchange/producto_tabla/{valor?}', 'RemisionesController@onchange_productos');

Route::get('/remisiones/onchange/conductor/{atributo?}/{informacion?}', 'RemisionesController@onchange_conductor');

Route::get('/remisiones/remisiones/exportar/pdf/{atributo?}', 'RemisionesController@exportar_pdf')->name('remisiones.pdf');
Route::get('/remisiones/remisiones/imprimir/{atributo?}', 'RemisionesController@imprimir')->name('remisiones.imprimir');









/*

Route::get('/generate/csv/{fechaInicial?}/{fechaFinal?}/{conferencia?}/{asociado?}/{regional?}', 'ReportesController@exportCSVTodo');
Route::post('/reportes/exportTodoPdf', 'ReportesController@exportPDFTodo')->name('reportes.exportTodoPdf');

*/
Route::post('/reportes/generatemensajes', 'ReportesController@generatemensajes')->name('reportes.generatemensajes');
Route::post('/reportes/export/mensajes', 'ReportesController@exportMensajesCSV')->name('reportes.export.mensajes');
Route::post('/reportes/export/mensajespdf', 'ReportesController@exportMensajesPDF')->name('reportes.export.mensajespdf');




Route::resource('/user', 'UserController', ['except' => [
        'create', 'store']])->names('user');
