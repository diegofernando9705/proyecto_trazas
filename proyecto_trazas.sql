-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-01-2021 a las 23:38:00
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyecto_trazas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(71, '2014_10_12_000000_create_users_table', 1),
(72, '2014_10_12_100000_create_password_resets_table', 1),
(73, '2019_08_19_000000_create_failed_jobs_table', 1),
(74, '2020_03_31_113105_create_roles_table', 1),
(75, '2020_03_31_113548_create_role_user_table', 1),
(76, '2020_04_06_125005_create_permissions_table', 1),
(77, '2020_04_06_125249_create_permission_role_table', 1),
(78, '2021_01_06_233052_create_tabla_remisiones_table', 1),
(79, '2021_01_06_235217_create_tabla_descripcion_remisiones_table', 1),
(80, '2021_01_07_000827_create_tabla_detalle_descripcion_remisiones_table', 1),
(81, '2021_01_07_001012_create_tabla_productos_remisiones_table', 1),
(82, '2021_01_07_001303_create_tabla_clientes_remisiones_table', 1),
(83, '2021_01_07_002041_create_tabla_tipo_identificacion_table', 1),
(84, '2021_01_07_002315_create_tabla_conductor_remision_table', 1),
(85, '2021_01_07_002743_create_tabla_parametros_remision_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Listar role', 'role.index', 'El usuario puede list role', '2021-01-08 02:33:44', '2021-01-08 02:33:44'),
(2, 'Ver rol', 'role.show', 'El usuario puede see role', '2021-01-08 02:33:44', '2021-01-08 02:33:44'),
(3, 'Crear rol', 'role.create', 'El usuario puede create role', '2021-01-08 02:33:45', '2021-01-08 02:33:45'),
(4, 'Editar rol', 'role.edit', 'El usuario puede edit role', '2021-01-08 02:33:45', '2021-01-08 02:33:45'),
(5, 'Eliminar rol', 'role.destroy', 'El usuario puede destroy role', '2021-01-08 02:33:45', '2021-01-08 02:33:45'),
(6, 'Listar Usuario', 'user.index', 'El usuario puede list user', '2021-01-08 02:33:45', '2021-01-08 02:33:45'),
(7, 'Ver usuario', 'user.show', 'El usuario puede see user', '2021-01-08 02:33:45', '2021-01-08 02:33:45'),
(8, 'Editar usuario', 'user.edit', 'El usuario puede edit user', '2021-01-08 02:33:45', '2021-01-08 02:33:45'),
(9, 'Eliminar usuario', 'user.destroy', 'El usuario puede destroy user', '2021-01-08 02:33:45', '2021-01-08 02:33:45'),
(10, 'Ver own usuario', 'userown.show', 'El usuario puede see own user', '2021-01-08 02:33:45', '2021-01-08 02:33:45'),
(11, 'Editar own usuario', 'userown.edit', 'El usuario puede edit own user', '2021-01-08 02:33:45', '2021-01-08 02:33:45'),
(12, 'Create user', 'user.create', 'El usuario puede create user', '2021-01-08 02:33:45', '2021-01-08 02:33:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_role`
--

CREATE TABLE `permission_role` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `full-access` enum('yes','no') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `full-access`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', 'Administrator', 'yes', '2021-01-08 02:33:44', '2021-01-08 02:33:44'),
(2, 'Usuario registrado', 'usuarioregistrado', 'Usuario registrado en el sistema', 'no', '2021-01-08 02:33:44', '2021-01-08 02:33:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_user`
--

CREATE TABLE `role_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2021-01-08 02:33:44', '2021-01-08 02:33:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tabla_clientes_remisiones`
--

CREATE TABLE `tabla_clientes_remisiones` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipo_id_cliente` int(11) NOT NULL,
  `identificacion` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombres_cliente` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidos_cliente` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion_cliente` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `ciudad_cliente` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono_fijo_cliente` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `celular_1_cliente` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `celular_2_cliente` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_cliente` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado_cliente` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tabla_clientes_remisiones`
--

INSERT INTO `tabla_clientes_remisiones` (`id`, `tipo_id_cliente`, `identificacion`, `nombres_cliente`, `apellidos_cliente`, `direccion_cliente`, `ciudad_cliente`, `telefono_fijo_cliente`, `celular_1_cliente`, `celular_2_cliente`, `email_cliente`, `estado_cliente`, `created_at`, `updated_at`) VALUES
(1, 2, '1234188670', 'Diego Fernando', 'Velez ', 'Calle 51 # 24C 41', 'Cali', NULL, '3135523082', NULL, 'diego@gmail.com', 1, NULL, NULL),
(2, 2, '94281939', 'Jorge ', 'Velez ', 'Cra 32 B # 44 21', 'Cali', NULL, '3178430551', NULL, 'joevel@hotmail.com', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tabla_conductor_remision`
--

CREATE TABLE `tabla_conductor_remision` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_tipo_conductor` int(11) NOT NULL,
  `identificacion_conductor` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombres_conductor` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidos_conductor` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `celular_1_conductor` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `celular_2_conductor` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `placa_conductor` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado_conductor` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tabla_conductor_remision`
--

INSERT INTO `tabla_conductor_remision` (`id`, `id_tipo_conductor`, `identificacion_conductor`, `nombres_conductor`, `apellidos_conductor`, `celular_1_conductor`, `celular_2_conductor`, `placa_conductor`, `estado_conductor`, `created_at`, `updated_at`) VALUES
(1, 2, '12341888670', 'Jairo ', 'Mostaquiva ', '3006227855', NULL, 'QFH31E', 1, NULL, NULL),
(2, 2, '942819636', 'Prueba ', 'Otra ', '317843554545', NULL, 'NMD54B', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tabla_descripcion_remisiones`
--

CREATE TABLE `tabla_descripcion_remisiones` (
  `id_descripcion` int(10) UNSIGNED NOT NULL,
  `id_producto` int(11) NOT NULL,
  `certificado_de_origen` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `total` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tabla_descripcion_remisiones`
--

INSERT INTO `tabla_descripcion_remisiones` (`id_descripcion`, `id_producto`, `certificado_de_origen`, `cantidad`, `total`, `created_at`, `updated_at`) VALUES
(27, 1, 'DDDDD', NULL, NULL, NULL, NULL),
(28, 3, 'DASDS', NULL, NULL, NULL, NULL),
(29, 1, 'd', NULL, NULL, NULL, NULL),
(30, 2, 'd', NULL, NULL, NULL, NULL),
(31, 2, 'daqdsad', NULL, NULL, NULL, NULL),
(32, 3, 'adDAdad', NULL, NULL, NULL, NULL),
(33, 1, 'sadD', NULL, NULL, NULL, NULL),
(34, 2, 'adAD', NULL, NULL, NULL, NULL),
(35, 2, 'ADSasda', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tabla_detalle_descripcion_remisiones`
--

CREATE TABLE `tabla_detalle_descripcion_remisiones` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_remision_detalle` int(11) NOT NULL,
  `id_descripcion_remisiones` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tabla_detalle_descripcion_remisiones`
--

INSERT INTO `tabla_detalle_descripcion_remisiones` (`id`, `id_remision_detalle`, `id_descripcion_remisiones`, `created_at`, `updated_at`) VALUES
(3, 1, 27, NULL, NULL),
(4, 1, 28, NULL, NULL),
(5, 2, 29, NULL, NULL),
(6, 2, 30, NULL, NULL),
(7, 2, 31, NULL, NULL),
(8, 2, 32, NULL, NULL),
(9, 3, 33, NULL, NULL),
(10, 3, 34, NULL, NULL),
(11, 3, 35, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tabla_parametros_remision`
--

CREATE TABLE `tabla_parametros_remision` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_tipo_parametro` int(11) NOT NULL,
  `identificacion_parametros` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo_parametros` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre_parametros` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion_parametros` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_ciudad_parametros` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono_fijo_parametros` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `celular_1_parametros` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `celular_2_parametros` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `celular_3_parametros` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_cliente` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado_parametro` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tabla_parametros_remision`
--

INSERT INTO `tabla_parametros_remision` (`id`, `id_tipo_parametro`, `identificacion_parametros`, `logo_parametros`, `nombre_parametros`, `direccion_parametros`, `id_ciudad_parametros`, `telefono_fijo_parametros`, `celular_1_parametros`, `celular_2_parametros`, `celular_3_parametros`, `email_cliente`, `estado_parametro`, `created_at`, `updated_at`) VALUES
(1, 1, '900534545', 'icon-trazas.png', 'TRAZAS Y MINERALES DE COLOMBIA', 'PLANTA PRINCIPAL PASO DE LA BOLSA, VEREDA SANCHEZ / CALLEJON LA LUISA', 'JAMUNDI - VALLE', NULL, '315 315 7478', '316 282 0912', NULL, 'dianagomez@trazascol.com - info@trazascol.com', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tabla_productos_remisiones`
--

CREATE TABLE `tabla_productos_remisiones` (
  `id_producto` int(10) UNSIGNED NOT NULL,
  `descripcion_producto` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado_producto` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tabla_productos_remisiones`
--

INSERT INTO `tabla_productos_remisiones` (`id_producto`, `descripcion_producto`, `estado_producto`, `created_at`, `updated_at`) VALUES
(1, 'Prueba uno', 1, NULL, NULL),
(2, 'Prueba dos', 1, NULL, NULL),
(3, 'Prueba tres', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tabla_remisiones`
--

CREATE TABLE `tabla_remisiones` (
  `id_consecutivo` int(11) UNSIGNED NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `fecha` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vence` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orden` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_conductor_remision` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtotal` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tabla_remisiones`
--

INSERT INTO `tabla_remisiones` (`id_consecutivo`, `id_cliente`, `fecha`, `vence`, `orden`, `id_conductor_remision`, `subtotal`, `total`, `estado`, `created_at`, `updated_at`) VALUES
(1, 1234188670, '2021-01-08', NULL, 'ZCOM DDH', '12341888670', NULL, NULL, 0, NULL, NULL),
(2, 94281939, '2021-01-08', NULL, 'AZCOMD', '12341888670', NULL, NULL, 1, NULL, NULL),
(3, 94281939, '2021-01-08', NULL, 'ZCOM Nro 48548484', '942819636', NULL, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tabla_tipo_identificacion`
--

CREATE TABLE `tabla_tipo_identificacion` (
  `id_tipo` int(10) UNSIGNED NOT NULL,
  `descripcion_tipo` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado_tipo` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tabla_tipo_identificacion`
--

INSERT INTO `tabla_tipo_identificacion` (`id_tipo`, `descripcion_tipo`, `estado_tipo`, `created_at`, `updated_at`) VALUES
(1, 'NIT', 1, NULL, NULL),
(2, 'Cedula', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', NULL, '$2y$10$z3XogRYuEeCkbVEkqfC4fuXWgeKCt9Qvey8CxBhoH8xR6ZCzpLuim', NULL, '2021-01-08 02:33:44', '2021-01-08 02:33:44');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`),
  ADD UNIQUE KEY `permissions_slug_unique` (`slug`);

--
-- Indices de la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`),
  ADD KEY `permission_role_permission_id_foreign` (`permission_id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indices de la tabla `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `tabla_clientes_remisiones`
--
ALTER TABLE `tabla_clientes_remisiones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tabla_clientes_remisiones_email_cliente_unique` (`email_cliente`) USING HASH;

--
-- Indices de la tabla `tabla_conductor_remision`
--
ALTER TABLE `tabla_conductor_remision`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tabla_descripcion_remisiones`
--
ALTER TABLE `tabla_descripcion_remisiones`
  ADD PRIMARY KEY (`id_descripcion`);

--
-- Indices de la tabla `tabla_detalle_descripcion_remisiones`
--
ALTER TABLE `tabla_detalle_descripcion_remisiones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tabla_parametros_remision`
--
ALTER TABLE `tabla_parametros_remision`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tabla_parametros_remision_email_cliente_unique` (`email_cliente`) USING HASH;

--
-- Indices de la tabla `tabla_productos_remisiones`
--
ALTER TABLE `tabla_productos_remisiones`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indices de la tabla `tabla_remisiones`
--
ALTER TABLE `tabla_remisiones`
  ADD PRIMARY KEY (`id_consecutivo`);

--
-- Indices de la tabla `tabla_tipo_identificacion`
--
ALTER TABLE `tabla_tipo_identificacion`
  ADD PRIMARY KEY (`id_tipo`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tabla_clientes_remisiones`
--
ALTER TABLE `tabla_clientes_remisiones`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tabla_conductor_remision`
--
ALTER TABLE `tabla_conductor_remision`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tabla_descripcion_remisiones`
--
ALTER TABLE `tabla_descripcion_remisiones`
  MODIFY `id_descripcion` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `tabla_detalle_descripcion_remisiones`
--
ALTER TABLE `tabla_detalle_descripcion_remisiones`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `tabla_parametros_remision`
--
ALTER TABLE `tabla_parametros_remision`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tabla_productos_remisiones`
--
ALTER TABLE `tabla_productos_remisiones`
  MODIFY `id_producto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tabla_remisiones`
--
ALTER TABLE `tabla_remisiones`
  MODIFY `id_consecutivo` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `tabla_tipo_identificacion`
--
ALTER TABLE `tabla_tipo_identificacion`
  MODIFY `id_tipo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
